# CS 373 - IDB 09

## Group Information
| Name               | EID     | GitLab     |
| ------------------ | ------- | ---------- |
| Susmita Renjith    | sr49295 | srenjith   |
| Rashad Philiazire  | rlp2988 | rashadphil |
| Darren Slack       | ds53747 | Dartu42    |
| Vrishank Viswanath | vkv297  | vrishank   |
| Zachary Guo        | zlg264  | zachlguo   |

## Project Information
**Git SHA**: 706d062f32da1eac9b7c6a190688eb34f2c0a5ac

**Project Leader**: Darren Slack

**Leader Roles**: 
- Determine timeline for completing phase requirements
- Assign tasks to other group members
- Coordinate group meetings

**GitLab Pipelines**: https://gitlab.com/srenjith/cs373-idb-09/-/pipelines

**Website Link**: https://campuseatsandtreats.web.app/

### Completion Time Estimates
| Name               | Estimated  | Actual    |
| ------------------ | ---------- | --------- |
| Susmita Renjith    | 10 hours   | 10 hours  |
| Rashad Philiazire  | 12 hours   | 10 hours  |
| Darren Slack       | 9 hours   | 10 hours  |
| Vrishank Viswanath | 13 hours   | 10 hours  |
| Zachary Guo        | 8 hours   | 10 hours  |

## Comments
- Some of our code was referenced from other projects or sources:
  - `guitests.py` was referenced from https://gitlab.com/sriyab/workitout/-/blob/main/front-end/guitest.py
  - `.gitlab-ci.yml` was referenced from https://gitlab.com/10AMGroup11/bookrus/-/blob/main/.gitlab-ci.yml
  - `assetTransformer.js` was referenced from https://github.com/facebook/jest/issues/2663#issuecomment-317109798