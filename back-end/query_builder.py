from flask_restful import reqparse
from sqlalchemy import or_


class DefaultRequestParser:
    def __init__(
        self,
        sortable_fields: list,
        exact_filterable_fields: list,
        range_filterable_fields: list,
    ) -> None:
        self.parser = reqparse.RequestParser()
        self.add_pagination_args()

        self.add_sort_args(sortable_fields)
        self.add_exact_filter_args(exact_filterable_fields)
        self.add_range_filter_args(range_filterable_fields)
        self.add_search_arg()

    def add_pagination_args(self):
        self.parser.add_argument("page", type=int, default=1, location="args")
        self.parser.add_argument(
            "per_page", type=int, default=12, location="args", choices=range(1, 101)
        )
        self.parser.add_argument("all_items", type=bool, default=False, location="args")

    def add_sort_args(self, sortable_fields):
        field_names = [field.name for field in sortable_fields]

        asc_fields = [f"{field}.asc" for field in field_names]
        desc_fields = [f"{field}.desc" for field in field_names]

        self.parser.add_argument(
            "sort_by",
            type=str,
            default="id.asc",
            location="args",
            choices=asc_fields + desc_fields,
        )

    def add_exact_filter_args(self, exact_filterable_fields):
        field_names = [field.name for field in exact_filterable_fields]

        for field in field_names:
            self.parser.add_argument(field, type=str, location="args")

    def add_range_filter_args(self, range_filterable_fields):
        field_names = [field.name for field in range_filterable_fields]

        for field in field_names:
            self.parser.add_argument(f"{field}.min", type=str, location="args")
            self.parser.add_argument(f"{field}.max", type=str, location="args")

    def add_search_arg(self):
        self.parser.add_argument("q", type=str, location="args")

    def parse_args(self):
        return self.parser.parse_args()


class QueryBuilder:
    def __init__(
        self, model, args, sortables, exact_filterables, range_filterables, searchables
    ) -> None:
        self.query = model.query
        self.model = model
        self.args = args

        self.sortable_fields = sortables
        self.exact_filterable_fields = exact_filterables
        self.range_filterable_fields = range_filterables
        self.searchable_fields = searchables

        self.apply_sorting()
        self.apply_exact_filters()
        self.apply_range_filters()
        self.apply_search()

    def paginate(self):
        per_page = self.args["per_page"]
        if self.args["all_items"]:
            per_page = self.query.count()

        return self.query.paginate(
            page=self.args["page"],
            per_page=per_page,
            error_out=False,
        )

    def apply_sorting(self):
        sort_by, sort_order = self.args["sort_by"].split(".")
        sort_attr = getattr(self.model, sort_by)

        if sort_order == "asc":
            self.query = self.query.order_by(sort_attr.asc())
        else:
            self.query = self.query.order_by(sort_attr.desc())

    def apply_exact_filters(self):
        filters = []
        filterable_field_names = [field.name for field in self.exact_filterable_fields]

        for field in self.args:
            value = self.args[field]

            if value and field in filterable_field_names:
                field_attr = getattr(self.model, field)
                filters.append(field_attr == value)

        self.query = self.query.filter(*filters)

    def apply_range_filters(self):
        filters = []

        filterable_field_names = []
        for field in self.range_filterable_fields:
            filterable_field_names.append(f"{field.name}.min")
            filterable_field_names.append(f"{field.name}.max")

        for field in self.args:
            if field in filterable_field_names:
                name, min_or_max = field.split(".")
                value = self.args[field]

                if value:
                    field_attr = getattr(self.model, name)

                    if min_or_max == "min":
                        filters.append(field_attr >= value)
                    else:
                        filters.append(field_attr <= value)

        self.query = self.query.filter(*filters)

    def apply_search(self):
        search_query = self.args["q"]
        if not search_query:
            return

        filters = []
        for field in self.searchable_fields:
            filters.append(field.ilike(f"%{search_query}%"))

        self.query = self.query.filter(or_(*filters))
