import populate.event
from query_builder import DefaultRequestParser, QueryBuilder
from flask_restful import Api, Resource, reqparse

from models.event import Event, event_schema, events_schema

sortable_fields = [
    Event.name,
    Event.venue_name,
    Event.start_date,
    Event.min_price,
    Event.max_price,
]

exact_filterable_fields = [
    Event.city,
    Event.state,
    Event.venue_name,
    Event.country,
    Event.start_date,
    Event.classification,
]

range_filterable_fields = [
    Event.min_price,
    Event.max_price,
    Event.start_date,
]

searchable_fields = [
    Event.name,
    Event.city,
    Event.state,
    Event.venue_name,
    Event.country,
    Event.classification,
]


class EventPopulateResource(Resource):
    def post(self):
        populate.event.populate_events()
        return "Events added to database"


class EventListResource(Resource):
    def __init__(self) -> None:
        self.parser = DefaultRequestParser(
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
        )

    def get(self):
        args = self.parser.parse_args()

        query = QueryBuilder(
            Event,
            args,
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
            searchable_fields,
        )

        paginate = query.paginate()
        events = paginate.items

        metadata = {
            "page": paginate.page,
            "per_page": paginate.per_page,
            "total_pages": paginate.pages,
            "total_items": paginate.total,
        }

        return {"metadata": metadata, "results": events_schema.dump(events)}


class EventResource(Resource):
    def get(self, event_id):
        event = Event.query.get_or_404(event_id)
        return event_schema.dump(event)
