import populate.college
from flask_restful import Resource, reqparse

from models.college import College, college_schema, colleges_schema

from query_builder import DefaultRequestParser, QueryBuilder

sortable_fields = [
    College.name,
    College.admission_rate,
    College.size,
    College.in_state_tuition,
    College.out_of_state_tuition,
    College.sat_score,
]

exact_filterable_fields = [College.state, College.city]

range_filterable_fields = [
    College.admission_rate,
    College.size,
    College.in_state_tuition,
    College.out_of_state_tuition,
    College.sat_score,
]

searchable_fields = [College.name, College.city, College.state]


class CollegePopulateResource(Resource):
    def post(self):
        populate.college.populate_colleges()
        return "Colleges added to database"


class CollegeListResource(Resource):
    def __init__(self) -> None:
        self.parser = DefaultRequestParser(
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
        )

    def get(self):
        args = self.parser.parse_args()

        query = QueryBuilder(
            College,
            args,
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
            searchable_fields,
        )

        paginate = query.paginate()
        colleges = paginate.items

        metadata = {
            "page": paginate.page,
            "per_page": paginate.per_page,
            "total_pages": paginate.pages,
            "total_items": paginate.total,
        }
        return {"metadata": metadata, "results": colleges_schema.dump(colleges)}


class CollegeResource(Resource):
    def get(self, college_id):
        college = College.query.get_or_404(college_id)
        return college_schema.dump(college)


class PopulateImageResource(Resource):
    def __init__(self) -> None:
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("model_type", type=str, location="args", required=True)
        super().__init__()

    def post(self):
        args = self.parser.parse_args()
        model_type = args["model_type"]
        if model_type == "college":
            populate.images.populate_images(College, search_ext="best view")
        else:
            return "Invalid model type", 400
        return "Images added to database"
