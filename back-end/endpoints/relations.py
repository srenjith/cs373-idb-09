from flask_restful import Resource

from models.college import College, college_schema
from models.restaurant import Restaurant, restaurant_schema
from models.event import Event, event_schema
from models.related_college_restaurant import (
    RelatedCollegeRestaurant,
)
from models.related_restaurant_event import (
    RelatedRestaurantEvent,
)
from models.related_college_event import (
    RelatedCollegeEvent,
)


class CollegeRelatedRestaurantsResource(Resource):
    def get(self, college_id):
        related_restaurants = (
            Restaurant.query.join(RelatedCollegeRestaurant)
            .filter(RelatedCollegeRestaurant.college_id == college_id)
            .order_by(RelatedCollegeRestaurant.distance)
            .limit(5)
            .all()
        )
        results = []
        for restaurant in related_restaurants:
            results.append(
                {
                    "restaurant": restaurant_schema.dump(restaurant),
                    "distance": RelatedCollegeRestaurant.query.filter_by(
                        college_id=college_id, restaurant_id=restaurant.id
                    )
                    .first()
                    .distance,
                }
            )
        return results


class RestaurantRelatedCollegesResource(Resource):
    def get(self, restaurant_id):
        related_colleges = (
            College.query.join(RelatedCollegeRestaurant)
            .filter(RelatedCollegeRestaurant.restaurant_id == restaurant_id)
            .order_by(RelatedCollegeRestaurant.distance)
            .limit(5)
            .all()
        )
        results = []
        for college in related_colleges:
            results.append(
                {
                    "college": college_schema.dump(college),
                    "distance": RelatedCollegeRestaurant.query.filter_by(
                        restaurant_id=restaurant_id, college_id=college.id
                    )
                    .first()
                    .distance,
                }
            )
        return results


class RestaurantRelatedEventsResource(Resource):
    def get(self, restaurant_id):
        related_events = (
            Event.query.join(RelatedRestaurantEvent)
            .filter(RelatedRestaurantEvent.restaurant_id == restaurant_id)
            .order_by(RelatedRestaurantEvent.distance)
            .limit(5)
            .all()
        )
        results = []
        for event in related_events:
            results.append(
                {
                    "event": event_schema.dump(event),
                    "distance": RelatedRestaurantEvent.query.filter_by(
                        restaurant_id=restaurant_id, event_id=event.id
                    )
                    .first()
                    .distance,
                }
            )
        return results


class EventRelatedRestaurantsResource(Resource):
    def get(self, event_id):
        related_restaurants = (
            Restaurant.query.join(RelatedRestaurantEvent)
            .filter(RelatedRestaurantEvent.event_id == event_id)
            .order_by(RelatedRestaurantEvent.distance)
            .limit(5)
            .all()
        )
        results = []
        for restaurant in related_restaurants:
            results.append(
                {
                    "restaurant": restaurant_schema.dump(restaurant),
                    "distance": RelatedRestaurantEvent.query.filter_by(
                        event_id=event_id, restaurant_id=restaurant.id
                    )
                    .first()
                    .distance,
                }
            )
        return results


class CollegeRelatedEventsResource(Resource):
    def get(self, college_id):
        related_events = (
            Event.query.join(RelatedCollegeEvent)
            .filter(RelatedCollegeEvent.college_id == college_id)
            .order_by(RelatedCollegeEvent.distance)
            .limit(5)
            .all()
        )
        results = []
        for event in related_events:
            results.append(
                {
                    "event": event_schema.dump(event),
                    "distance": RelatedCollegeEvent.query.filter_by(
                        college_id=college_id, event_id=event.id
                    )
                    .first()
                    .distance,
                }
            )
        return results


class EventRelatedCollegesResource(Resource):
    def get(self, event_id):
        related_colleges = (
            College.query.join(RelatedCollegeEvent)
            .filter(RelatedCollegeEvent.event_id == event_id)
            .order_by(RelatedCollegeEvent.distance)
            .limit(5)
            .all()
        )
        results = []
        for college in related_colleges:
            results.append(
                {
                    "college": college_schema.dump(college),
                    "distance": RelatedCollegeEvent.query.filter_by(
                        event_id=event_id, college_id=college.id
                    )
                    .first()
                    .distance,
                }
            )
        return results
