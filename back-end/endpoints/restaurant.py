from query_builder import DefaultRequestParser, QueryBuilder
import populate.restaurant
from flask_restful import Resource, reqparse

from models.restaurant import Restaurant, restaurant_schema, restaurants_schema

sortable_fields = [Restaurant.name, Restaurant.rating, Restaurant.price]

exact_filterable_fields = [
    Restaurant.city,
    Restaurant.state,
    Restaurant.category,
    Restaurant.price,
]

range_filterable_fields = [Restaurant.rating, Restaurant.price]

searchable_fields = [
    Restaurant.name,
    Restaurant.city,
    Restaurant.state,
    Restaurant.category,
]


class RestaurantPopulateResource(Resource):
    def post(self):
        populate.restaurant.populate_restaurants()
        return "Restaurants added to database"


class RestaurantListResource(Resource):
    def __init__(self) -> None:
        self.parser = DefaultRequestParser(
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
        )

    def get(self):
        args = self.parser.parse_args()

        query = QueryBuilder(
            Restaurant,
            args,
            sortable_fields,
            exact_filterable_fields,
            range_filterable_fields,
            searchable_fields,
        )

        paginate = query.paginate()
        restaurants = paginate.items

        metadata = {
            "page": paginate.page,
            "per_page": paginate.per_page,
            "total_pages": paginate.pages,
            "total_items": paginate.total,
        }

        return {"metadata": metadata, "results": restaurants_schema.dump(restaurants)}


class RestaurantResource(Resource):
    def get(self, restaurant_id):
        restaurant = Restaurant.query.get_or_404(restaurant_id)
        return restaurant_schema.dump(restaurant)
