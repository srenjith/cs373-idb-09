from typing import List
from unittest import main, TestCase
import unittest
from models.college import College
from init import app
import app as main

VALID_COLLEGE_ID = 700
INVALID_COLLEGE_ID = 9999

VALID_RESTAURANT_ID = 5
INVALID_RESTAURANT_ID = 9999

VALID_EVENT_ID = 1
INVALID_EVENT_ID = 9999


class UnitTests(TestCase):
    def setUp(self):
        self.app = app
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

    def test_1(self):
        # Test that the endpoint returns a list of colleges
        response = self.client.get("/colleges")
        self.assertEqual(response.status_code, 200)

        data = response.json

        self.assertIn("metadata", data)
        self.assertIsInstance(data["results"], List)

        num_results = len(data["results"])
        self.assertGreater(num_results, 0)
        self.assertLessEqual(num_results, data["metadata"]["per_page"])

        # make sure all colleges are unique
        college_ids = [college["id"] for college in data["results"]]
        self.assertEqual(len(college_ids), len(set(college_ids)))

    def test_2(self):
        # Test that the endpoint returns a list of restaurants
        response = self.client.get("/restaurants")
        self.assertEqual(response.status_code, 200)

        data = response.json

        self.assertIn("metadata", data)
        self.assertIsInstance(data["results"], List)

        num_results = len(data["results"])
        self.assertGreater(num_results, 0)
        self.assertLessEqual(num_results, data["metadata"]["per_page"])

        # make sure all restaurants are unique
        restaurant_ids = [restaurant["id"] for restaurant in data["results"]]
        self.assertEqual(len(restaurant_ids), len(set(restaurant_ids)))

    def test_3(self):
        # Test that the endpoint returns a list of events
        response = self.client.get("/events")
        self.assertEqual(response.status_code, 200)

        data = response.json

        self.assertIn("metadata", data)
        self.assertIsInstance(data["results"], List)

        num_results = len(data["results"])
        self.assertGreater(num_results, 0)
        self.assertLessEqual(num_results, data["metadata"]["per_page"])

        # make sure all events are unique
        event_ids = [event["id"] for event in data["results"]]
        self.assertEqual(len(event_ids), len(set(event_ids)))

    def test_4(self):
        # Test individual college endpoint
        response = self.client.get(f"/colleges/{VALID_COLLEGE_ID}")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, dict)
        self.assertEqual(data["id"], VALID_COLLEGE_ID)

    def test_5(self):
        # Test individual restaurant endpoint
        response = self.client.get(f"/restaurants/{VALID_RESTAURANT_ID}")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, dict)
        self.assertEqual(data["id"], VALID_RESTAURANT_ID)

    def test_6(self):
        # Test individual event endpoint
        response = self.client.get(f"/events/{VALID_EVENT_ID}")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, dict)
        self.assertEqual(data["id"], VALID_EVENT_ID)

    def test_7(self):
        # Test individual college endpoint with invalid id
        response = self.client.get(f"/colleges/{INVALID_COLLEGE_ID}")
        self.assertEqual(response.status_code, 404)

    def test_8(self):
        # Test individual restaurant endpoint with invalid id
        response = self.client.get(f"/restaurants/{INVALID_RESTAURANT_ID}")
        self.assertEqual(response.status_code, 404)

    def test_9(self):
        # Test individual event endpoint with invalid id
        response = self.client.get(f"/events/{INVALID_EVENT_ID}")
        self.assertEqual(response.status_code, 404)

    def test_10(self):
        # Test nearby restaurants to college endpoint
        response = self.client.get(f"/colleges/{VALID_COLLEGE_ID}/restaurants")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, list)

        instance = data[0]
        self.assertIn("distance", instance)
        self.assertIn("restaurant", instance)

    def test_11(self):
        # Test nearby events to restaurant endpoint
        response = self.client.get(f"/restaurants/{VALID_RESTAURANT_ID}/events")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, list)

        instance = data[0]
        self.assertIn("distance", instance)
        self.assertIn("event", instance)

    def test_12(self):
        # Test nearby restaurants to event endpoint
        response = self.client.get(f"/events/{VALID_EVENT_ID}/restaurants")
        self.assertEqual(response.status_code, 200)

        data = response.json
        self.assertIsInstance(data, list)

        instance = data[0]
        self.assertIn("distance", instance)
        self.assertIn("restaurant", instance)


if __name__ == "__main__":
    unittest.main()
