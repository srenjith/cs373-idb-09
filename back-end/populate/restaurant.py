from typing import Iterable
import requests
import os


from init import db
from models.restaurant import Restaurant
from models.college import College

YELP_API_URL = "https://api.yelp.com/v3/businesses/search"

default_params = {
    "term": "food",
    "radius": 2000,
    "sort_by": "review_count",
}

headers = {
    "Authorization": "Bearer " + os.environ.get("YELP_API_KEY"),
}


def is_valid_restaurant(restaurant: Restaurant) -> bool:
    fields = [
        restaurant.name,
        restaurant.address,
        restaurant.city,
        restaurant.state,
        restaurant.latitude,
        restaurant.longitude,
        restaurant.phone,
        restaurant.price,
        restaurant.rating,
        restaurant.image,
    ]

    return all(fields)


def fetch_coordinates_restaurants(longitude: float, latitude: float):
    params = default_params.copy()
    params["longitude"] = longitude
    params["latitude"] = latitude
    response = requests.get(YELP_API_URL, params=params, headers=headers)
    data = response.json()
    return data["businesses"]


def build_restaurant_list() -> Iterable[Restaurant]:
    RESTAURANTS_PER_COLLEGE = 3
    all_coordinates = College.all_coordinates()
    restaurant_data = []

    for i, (long, lat) in enumerate(all_coordinates):
        restaurants = fetch_coordinates_restaurants(long, lat)
        restaurant_data.extend(restaurants[:RESTAURANTS_PER_COLLEGE])
        print(f"Coordinates Processed: {i + 1}/{len(all_coordinates)}")

    restaurants = map(Restaurant, restaurant_data)
    restaurants = filter(is_valid_restaurant, restaurants)

    return restaurants


def populate_restaurants():
    restaurants = build_restaurant_list()

    # Add colleges to database, if they don't already exist
    for i, restaurant in enumerate(restaurants):
        print(f"Restaurants processed: {i + 1}")
        if not Restaurant.query.filter_by(name=restaurant.name).first():
            db.session.add(restaurant)
            db.session.commit()

    print("Restaurants added to database")
