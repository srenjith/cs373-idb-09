import requests
import os

from app import db

BING_SEARCH_URL = "https://api.bing.microsoft.com/v7.0/images/search"
params = {
    "q": "",
    "count": 1,
    "mkt": "en-US",
    "safeSearch": "Strict",
    "aspect": "wide",
}

headers = {
    "Ocp-Apim-Subscription-Key": os.environ.get("BING_SEARCH_KEY"),
}


def fetch_image(search_term: str):
    params["q"] = search_term
    json = requests.get(BING_SEARCH_URL, params=params, headers=headers).json()

    if json["value"]:
        return json["value"][0]["contentUrl"]

    return None


def populate_images(model: db.Model, search_ext: str = ""):
    print(f"Populating images for {model.__name__}")
    for item in model.query.all():
        if item.image is not None and item.image != "":
            continue

        search_term = item.name + " " + search_ext
        item.image = fetch_image(search_term)

        db.session.commit()

    print("Images added to database")
