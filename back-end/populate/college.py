from typing import Iterable
import requests
import os

from init import db
from models.college import College

COLLEGE_API_URL = "https://api.data.gov/ed/collegescorecard/v1/schools"

college_fields = [
    "id",
    "school.name",
    "2020.student.size",
    "school.city",
    "school.state",
    "admission_rate.overall",
    "tuition.in_state",
    "tuition.out_of_state",
    "2020.admissions.admission_rate.overall",
    "2020.admissions.sat_scores.average.overall",
    "2020.cost.tuition.in_state",
    "2020.cost.tuition.out_of_state",
    "location.lon",
    "location.lat",
]

default_params = {
    "fields": ",".join(college_fields),
    "sort": "admissions.sat_scores.average.overall:desc",
    "api_key": os.environ.get("COLLEGE_API_KEY"),
    "per_page": 100,
}


def is_valid_college(college) -> bool:
    fields = [
        college.name,
        college.city,
        college.state,
        college.admission_rate,
        college.in_state_tuition,
        college.out_of_state_tuition,
        college.sat_score,
        college.longitude,
        college.latitude,
    ]

    return all(fields)


def fetch_college_page(page_num: int):
    params = default_params.copy()
    params["page"] = page_num
    response = requests.get(COLLEGE_API_URL, params=params)
    data = response.json()
    return data["results"]


def build_college_list() -> Iterable[College]:
    NUM_PAGES = 4
    college_data = []
    for page in range(0, NUM_PAGES):  # 4 pages of 100 colleges (400 colleges)
        college_page = fetch_college_page(page)
        college_data.extend(college_page)
        print(f"Page {page} of {NUM_PAGES} complete")

    colleges = map(College, college_data)
    colleges = filter(is_valid_college, colleges)
    return colleges


def populate_colleges():
    colleges = build_college_list()

    # Add colleges to database, if they don't already exist
    for college in colleges:
        if not College.query.filter_by(name=college.name).first():
            db.session.add(college)

    db.session.commit()

    print("Colleges added to database")
