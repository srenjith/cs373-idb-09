from typing import Iterator
from init import db
from models.college import College
from models.event import Event
from models.related_college_event import RelatedCollegeEvent
import geopy.distance


def distance(place1, place2):
    lat1, lon1 = place1.latitude, place1.longitude
    lat2, lon2 = place2.latitude, place2.longitude
    return geopy.distance.distance((lat1, lon1), (lat2, lon2)).miles


def college_event_iter() -> Iterator[RelatedCollegeEvent]:
    colleges = College.query.all()
    events = Event.query.all()

    for college in colleges:
        for event in events:
            dist = distance(college, event)
            yield RelatedCollegeEvent(
                college_id=college.id, event_id=event.id, distance=dist
            )


def populate_related_college_event():
    college_events = college_event_iter()
    college_events = filter(lambda x: x.distance < 20, college_events)
    db.session.add_all(college_events)
    db.session.commit()
