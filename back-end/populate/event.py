from typing import Iterable
import os
import pygeohash as pgh
import requests

from init import db
from models.college import College
from models.event import Event

TICKETMASTER_API_URL = "https://app.ticketmaster.com/discovery/v2/events.json?"

default_params = {
    "apikey": os.environ.get("TICKETMASTER_API_KEY"),
}


def is_valid_event(event: Event) -> bool:
    fields = [
        event.name,
        event.longitude,
        event.latitude,
        event.url,
        event.image,
        event.start_date,
        event.classification,
        event.min_price,
        event.max_price,
        event.city,
        event.state,
        event.country,
        event.address,
        event.ticket_master_id,
    ]

    return all(fields)


def fetch_coordinates_events(longitude: float, latitude: float):
    params = default_params.copy()

    geohash = pgh.encode(latitude, longitude, precision=5)
    params["geoPoint"] = geohash
    response = requests.get(TICKETMASTER_API_URL, params=params)
    data = response.json()
    return data["_embedded"]["events"]


def iter_event_list() -> Iterable[Event]:
    EVENTS_PER_COLLEGE = 10
    all_coordinates = College.all_coordinates()

    for i, (long, lat) in enumerate(all_coordinates):
        events = fetch_coordinates_events(long, lat)
        events = events[:EVENTS_PER_COLLEGE]
        events = map(Event, events)
        events = filter(is_valid_event, events)

        for event in events:
            yield event
        print(f"Coordinates Processed: {i + 1}/{len(all_coordinates)}")


def populate_events():
    events = iter_event_list()

    for i, event in enumerate(events):
        print(f"Events processed: {i + 1}")
        if not Event.query.filter_by(name=event.name).first():
            db.session.add(event)
            db.session.commit()

    print("Events added to database")
