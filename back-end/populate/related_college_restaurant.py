from typing import Iterator
from app import db
from models.college import College
from models.related_college_restaurant import RelatedCollegeRestaurant
from models.restaurant import Restaurant
import geopy.distance


def distance(place1, place2):
    lat1, lon1 = place1.latitude, place1.longitude
    lat2, lon2 = place2.latitude, place2.longitude
    return geopy.distance.distance((lat1, lon1), (lat2, lon2)).miles


def college_restaurant_iter() -> Iterator[RelatedCollegeRestaurant]:
    restaurants = Restaurant.query.all()
    colleges = College.query.all()

    for restaurant in restaurants:
        for college in colleges:
            dist = distance(restaurant, college)
            yield RelatedCollegeRestaurant(
                college_id=college.id, restaurant_id=restaurant.id, distance=dist
            )


def populate_related_college_restaurant():
    college_restaurants = college_restaurant_iter()
    college_restaurants = filter(lambda x: x.distance < 60, college_restaurants)
    db.session.add_all(college_restaurants)
    db.session.commit()
