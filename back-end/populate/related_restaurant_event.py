from typing import Iterator
from app import db
from models.college import College
from models.event import Event
from models.related_restaurant_event import RelatedRestaurantEvent
from models.restaurant import Restaurant
import geopy.distance


def distance(place1, place2):
    lat1, lon1 = place1.latitude, place1.longitude
    lat2, lon2 = place2.latitude, place2.longitude
    return geopy.distance.distance((lat1, lon1), (lat2, lon2)).miles


def restaurant_event_iter() -> Iterator[RelatedRestaurantEvent]:
    restaurants = Restaurant.query.all()
    events = Event.query.all()

    for restaurant in restaurants:
        for event in events:
            dist = distance(restaurant, event)
            yield RelatedRestaurantEvent(
                restaurant_id=restaurant.id, event_id=event.id, distance=dist
            )


def populate_related_restaurant_event():
    restaurant_events = restaurant_event_iter()
    restaurant_events = filter(lambda x: x.distance < 20, restaurant_events)
    db.session.add_all(restaurant_events)
    db.session.commit()
