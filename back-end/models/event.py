from init import db, ma


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True, index=True)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    url = db.Column(db.Text)
    image = db.Column(db.Text)
    start_date = db.Column(db.DateTime)
    min_price = db.Column(db.Float)
    max_price = db.Column(db.Float)
    classification = db.Column(db.Text)
    city = db.Column(db.Text)
    state = db.Column(db.Text)
    country = db.Column(db.Text)
    address = db.Column(db.Text)
    ticket_master_id = db.Column(db.Text, index=True, unique=True)
    venue_name = db.Column(db.Text, nullable=True)

    def __init__(self, data):
        self.name = data["name"]
        self.url = data["url"]
        self.image = data["images"][0]["url"]
        self.start_date = data["dates"]["start"]["localDate"]

        if "priceRanges" in data:
            priceRange = data["priceRanges"][0]
            self.min_price = priceRange["min"]
            self.max_price = priceRange["max"]

        self.classification = data["classifications"][0]["segment"]["name"]
        self.ticket_master_id = data["id"]

        venue = data["_embedded"]["venues"][0]

        self.city = venue["city"]["name"]
        self.state = venue["state"]["name"]
        self.country = venue["country"]["name"]

        if "line1" in venue["address"]:
            self.address = venue["address"]["line1"]

        self.longitude = venue["location"]["longitude"]
        self.latitude = venue["location"]["latitude"]
        self.venue_name = venue["name"]

    def __repr__(self):
        return f"{self.name} ({self.city}, {self.state})"

    def __eq__(self, other):
        return self.ticket_master_id == other.ticket_master_id


class EventSchema(ma.Schema):
    class Meta:
        model = Event
        fields = (
            "id",
            "name",
            "longitude",
            "latitude",
            "description",
            "url",
            "image",
            "start_date",
            "classification",
            "min_price",
            "max_price",
            "city",
            "country",
            "state",
            "venue_name",
        )


event_schema = EventSchema()
events_schema = EventSchema(many=True)
