from init import db


class RelatedRestaurantEvent(db.Model):
    restaurant_id = db.Column(
        db.Integer, db.ForeignKey("restaurant.id"), primary_key=True
    )
    event_id = db.Column(db.Integer, db.ForeignKey("event.id"), primary_key=True)
    distance = db.Column(db.Float)

    def __init__(self, restaurant_id, event_id, distance):
        self.restaurant_id = restaurant_id
        self.event_id = event_id
        self.distance = distance

    def __repr__(self):
        return f"{self.restaurant_id} - {self.event_id} - {self.distance} distance"
