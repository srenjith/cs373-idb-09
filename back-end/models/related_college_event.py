from init import db


class RelatedCollegeEvent(db.Model):
    college_id = db.Column(db.Integer, db.ForeignKey("college.id"), primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey("event.id"), primary_key=True)
    distance = db.Column(db.Float)

    def __init__(self, college_id, event_id, distance):
        self.college_id = college_id
        self.event_id = event_id
        self.distance = distance

    def __repr__(self):
        return f"{self.college_id} - {self.event_id} - {self.distance} distance"
