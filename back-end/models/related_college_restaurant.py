from init import db


class RelatedCollegeRestaurant(db.Model):
    college_id = db.Column(db.Integer, db.ForeignKey("college.id"), primary_key=True)
    restaurant_id = db.Column(
        db.Integer, db.ForeignKey("restaurant.id"), primary_key=True
    )
    distance = db.Column(db.Float)

    def __init__(self, college_id, restaurant_id, distance):
        self.college_id = college_id
        self.restaurant_id = restaurant_id
        self.distance = distance

    def __repr__(self):
        return f"{self.college_id} - {self.restaurant_id} - {self.distance} distance"
