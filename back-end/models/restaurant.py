from init import db, ma


class Restaurant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True, index=True)
    url = db.Column(db.Text)
    image = db.Column(db.Text)
    rating = db.Column(db.Float)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    city = db.Column(db.Text)
    state = db.Column(db.Text)
    address = db.Column(db.Text)
    phone = db.Column(db.Text)
    price = db.Column(db.Integer)
    category = db.Column(db.Text)

    def __init__(self, data):
        self.name = data["name"]
        self.url = data["url"]
        self.image = data["image_url"]
        self.rating = data["rating"]
        self.longitude = data["coordinates"]["longitude"]
        self.latitude = data["coordinates"]["latitude"]
        self.city = data["location"]["city"]
        self.state = data["location"]["state"]
        self.address = data["location"]["address1"]
        self.phone = data["phone"]

        if "price" in data:
            self.price = len(data["price"])

        categories = data["categories"]
        self.category = categories[0]["title"] if categories else None

    def __repr__(self):
        return f"{self.name} ({self.city}, {self.state})"

    def __eq__(self, other):
        return self.name == other.name


class RestaurantSchema(ma.Schema):
    class Meta:
        model = Restaurant
        fields = (
            "id",
            "name",
            "url",
            "image",
            "rating",
            "longitude",
            "latitude",
            "city",
            "state",
            "address",
            "phone",
            "price",
            "category",
        )


restaurant_schema = RestaurantSchema()
restaurants_schema = RestaurantSchema(many=True)
