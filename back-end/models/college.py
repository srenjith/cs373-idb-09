from init import db, ma


class College(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True, index=True)
    size = db.Column(db.Integer)
    city = db.Column(db.Text)
    state = db.Column(db.Text)
    admission_rate = db.Column(db.Float)
    in_state_tuition = db.Column(db.Integer)
    out_of_state_tuition = db.Column(db.Integer)
    sat_score = db.Column(db.Integer)
    image = db.Column(db.Text)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)

    def __init__(self, data):
        self.name = data["school.name"]
        self.size = data["2020.student.size"]
        self.city = data["school.city"]
        self.state = data["school.state"]
        self.admission_rate = data["2020.admissions.admission_rate.overall"]
        self.in_state_tuition = data["2020.cost.tuition.in_state"]
        self.out_of_state_tuition = data["2020.cost.tuition.out_of_state"]
        self.sat_score = data["2020.admissions.sat_scores.average.overall"]
        self.longitude = data["location.lon"]
        self.latitude = data["location.lat"]
        self.image = None

    def __repr__(self):
        return f"{self.name} ({self.city}, {self.state})"

    def __eq__(self, other):
        return self.name == other.name

    @staticmethod
    def all_coordinates(limit=None):
        return db.session.query(College.longitude, College.latitude).limit(limit).all()


class CollegeSchema(ma.Schema):
    class Meta:
        model = College
        fields = (
            "id",
            "name",
            "size",
            "city",
            "state",
            "admission_rate",
            "in_state_tuition",
            "out_of_state_tuition",
            "sat_score",
            "longitude",
            "latitude",
            "image",
        )


college_schema = CollegeSchema()
colleges_schema = CollegeSchema(many=True)
