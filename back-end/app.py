import os
from endpoints.college import (
    CollegePopulateResource,
    CollegeListResource,
    CollegeResource,
    PopulateImageResource,
)

from endpoints.restaurant import (
    RestaurantPopulateResource,
    RestaurantListResource,
    RestaurantResource,
)

from endpoints.event import (
    EventPopulateResource,
    EventListResource,
    EventResource,
)

from endpoints.relations import (
    CollegeRelatedRestaurantsResource,
    RestaurantRelatedEventsResource,
    EventRelatedRestaurantsResource,
    RestaurantRelatedCollegesResource,
    CollegeRelatedEventsResource,
    EventRelatedCollegesResource,
)

from init import app, api


@app.route("/", methods=["GET", "POST"])
def welcome():
    return "Campus Eats and Treats"


api.add_resource(CollegeListResource, "/colleges")
api.add_resource(CollegeResource, "/colleges/<int:college_id>")
api.add_resource(
    CollegeRelatedRestaurantsResource, "/colleges/<int:college_id>/restaurants"
)
api.add_resource(CollegePopulateResource, "/populate_colleges")

api.add_resource(RestaurantListResource, "/restaurants")
api.add_resource(RestaurantResource, "/restaurants/<int:restaurant_id>")
api.add_resource(
    RestaurantRelatedEventsResource, "/restaurants/<int:restaurant_id>/events"
)
api.add_resource(
    RestaurantRelatedCollegesResource, "/restaurants/<int:restaurant_id>/colleges"
)
api.add_resource(RestaurantPopulateResource, "/populate_restaurants")

api.add_resource(EventListResource, "/events")
api.add_resource(EventResource, "/events/<int:event_id>")
api.add_resource(EventRelatedRestaurantsResource, "/events/<int:event_id>/restaurants")
api.add_resource(EventPopulateResource, "/populate_events")

api.add_resource(PopulateImageResource, "/populate_images")

api.add_resource(CollegeRelatedEventsResource, "/colleges/<int:college_id>/events")
api.add_resource(EventRelatedCollegesResource, "/events/<int:event_id>/colleges")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int(os.environ.get("PORT", 3000)))
