import SliderUnstyled, {
  SliderUnstyledThumbSlotProps,
  SliderUnstyledProps,
} from '@mui/base/SliderUnstyled';
import {forwardRef} from 'react';

export const RangeSlider = forwardRef(function Slider(
  props: SliderUnstyledProps,
  ref: React.ForwardedRef<HTMLSpanElement>
) {
  return (
    <SliderUnstyled
      {...props}
      ref={ref}
      slotProps={{
        thumb: {
          className:
            'ring-base-content ring-4 w-4 h-4 -mt-1 -ml-2 flex items-center justify-center bg-white rounded-full shadow absolute',
        },
        root: {className: 'w-full relative inline-block h-2 cursor-pointer'},
        rail: {
          className:
            'bg-gray-400 h-2 w-full rounded-full block absolute opacity-25',
        },
        track: {
          className: 'bg-base-content h-2 absolute rounded-full',
        },
      }}
    />
  );
});
