const Hero = () => {
  return (
    <div className="hero min-h-[95vh] px-5 bg-base-200">
      <div className="hero-content flex-col lg:flex-row-reverse">
        <div className="md:w-4/5 rounded-t-lg md:rounded-lg">
          <div className="h-full grid grid-rows-12 grid-cols-12 gap-4">
            <div className="row-start-1 row-end-3 col-start-1 col-end-8 rounded-md overflow-hidden">
              <img
                className="object-cover h-full w-full animate-fade-in-up"
                src="https://images.unsplash.com/photo-1547525372-7acfcbcd8acc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80"
                alt=""
              />
            </div>
            <div className="row-start-1 row-end-5 col-start-8 col-end-13 rounded-md overflow-hidden">
              <img
                className="object-cover h-full w-full animate-fade-in-up"
                src="https://images.unsplash.com/photo-1517457373958-b7bdd4587205?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2969&q=80"
                alt=""
              />
            </div>
            <div className="row-start-3 row-end-7 col-start-1 col-end-8 rounded-md overflow-hidden">
              <img
                className="object-cover h-full w-full animate-fade-in-up"
                src="https://images.unsplash.com/photo-1576867757603-05b134ebc379?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80"
                alt=""
              />
            </div>
            <div className="row-start-5 row-end-7 col-start-8 col-end-13 rounded-md overflow-hidden">
              <img
                className="object-cover h-full w-full animate-fade-in-up"
                src="https://images.unsplash.com/photo-1625283518288-00362afc8663?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDEyfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=800&q=60"
                alt=""
              />
            </div>
          </div>
        </div>
        <div>
          <h1 className="text-3xl md:text-5xl font-bold">
            Find something fun to do!
          </h1>
          <p className="py-4">
            With Campus Eats & Treats, it's easy to find colleges, restaurants,
            and events, all within a comfortable distance away.
          </p>
          <a href="/#explore">
            <button className="btn btn-primary">Let's go</button>
          </a>
        </div>
      </div>
    </div>
  );
};

export default Hero;
