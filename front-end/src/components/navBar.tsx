import {
  Bars3Icon,
  ChevronDownIcon,
  ChevronRightIcon,
} from '@heroicons/react/24/solid';
import {ChangeEvent, useEffect, useState} from 'react';
import {
  matchPath,
  NavLink,
  useHref,
  useLocation,
  useParams,
} from 'react-router-dom';
import SearchBar from './SearchBar';

const ModelLinks = ({styling}: {styling: string}) => {
  const modellinks = [
    {name: 'Colleges', path: '/colleges'},
    {name: 'Restaurants', path: '/restaurants'},
    {name: 'Events', path: '/events'},
  ];
  return (
    <ul className={styling}>
      {modellinks.map((link, index) => (
        <li key={index}>
          <NavLink
            to={link.path}
            className={({isActive}) => (isActive ? 'underline' : '')}
          >
            {link.name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};

const VisLinks = ({styling}: {styling: string}) => {
  const vislinks = [
    {name: 'Our Visualizations', path: '/our-visualizations'},
    {name: 'Provider Visualizations', path: '/provider-visualizations'},
  ];
  return (
    <ul className={styling}>
      {vislinks.map((link, index) => (
        <li key={index}>
          <NavLink
            to={link.path}
            className={({isActive}) => (isActive ? 'underline' : '')}
          >
            {link.name}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};

const NavBar = () => {
  const isExploreTab = ['/colleges', '/restaurants', '/events'].includes(
    useLocation().pathname
  );
  const isVisTab = ['/our-visualizations', '/provider-visualizations'].includes(
    useLocation().pathname
  );

  const [searchTerm, setSearchTerm] = useState('');
  const searchHandler = (e: ChangeEvent<HTMLInputElement>) =>
    setSearchTerm(e.target.value);

  const makeSearch = () => {
    if (searchTerm.length > 0) {
      window.location.href = `/search/${searchTerm}`;
    }
  };

  return (
    <div className="navbar bg-base-200 text-sm sticky top-0 z-50 px-8">
      <div className="flex-1">
        <div className="dropdown">
          <label tabIndex={0} className="btn btn-ghost md:hidden">
            <Bars3Icon className="w-6 h-6" />
          </label>
          <ul
            tabIndex={0}
            className="menu menu-compact dropdown-content mt-3 p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <NavLink
                to={'/'}
                className={({isActive}) => (isActive ? 'underline' : '')}
              >
                {'Home'}
              </NavLink>
            </li>
            <li>
              <NavLink
                to={'/about'}
                className={({isActive}) => (isActive ? 'underline' : '')}
              >
                {'About'}
              </NavLink>
            </li>
            <li tabIndex={0}>
              <a className={`justify-between ${isExploreTab && 'underline'}`}>
                Explore
                <ChevronRightIcon className="w-5 h-5" />
              </a>
              <ModelLinks styling="p-2 bg-base-100" />
            </li>
            <div className="border-2 rounded-lg">
              <SearchBar
                value={searchTerm}
                onChange={searchHandler}
                onEnter={makeSearch}
                placeholder={'Search'}
                nav={true}
              />
            </div>
          </ul>
        </div>
        <a href="/" className="btn btn-ghost normal-case text-xl font-bold">
          Campus Eats & Treats
        </a>
      </div>
      <div className="hidden md:flex">
        <ul className="menu menu-horizontal px-1 items-center">
          <li>
            <NavLink
              to={'/'}
              className={({isActive}) => (isActive ? 'underline' : '')}
            >
              {'Home'}
            </NavLink>
          </li>
          <li>
            <NavLink
              to={'/about'}
              className={({isActive}) => (isActive ? 'underline' : '')}
            >
              {'About'}
            </NavLink>
          </li>
          <li tabIndex={0}>
            <a className={`${isExploreTab && 'underline'}`}>
              Explore
              <ChevronDownIcon className="w-5 h-5" />
            </a>
            <ModelLinks styling="p-2 bg-base-100 z-50" />
          </li>
          <li tabIndex={0}>
            <a className={`${isVisTab && 'underline'}`}>
              Visualizations
              <ChevronDownIcon className="w-5 h-5" />
            </a>
            <VisLinks styling="p-2 bg-base-100 z-50" />
          </li>
          <SearchBar
            value={searchTerm}
            onChange={searchHandler}
            onEnter={makeSearch}
            placeholder={'Search'}
            nav
          />
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
