import {College} from '../api/API';
import DefaultCollege from '../assets/college_default.png';
import {UserIcon} from '@heroicons/react/24/solid';
import {getHighlightedText} from '../static/SearchSortFilterData';

const CollegeCard = ({
  college,
  search,
}: {
  college: College;
  search?: string;
}) => {
  return (
    <div className="bg-base-100 p-6 rounded-lg mx-auto hover:scale-95 transition-all animate-fade-in-up">
      <img
        className="h-40 rounded w-full object-cover object-center mb-6"
        src={college.image ? college.image : DefaultCollege}
        alt="content"
        onError={err => {
          (err.target as HTMLImageElement).src = DefaultCollege;
          (err.target as HTMLImageElement).onerror = null;
        }}
      />
      <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
        {getHighlightedText(search, college.city)},{' '}
        {getHighlightedText(search, college.state)}
      </h3>
      <h2 className="text-lg text-gray-900 font-medium title-font mb-2 truncate">
        {getHighlightedText(search, college.name)}
      </h2>
      <div className="flex w-full justify-between font-semibold text-sm mb-1">
        <div className="flex items-center space-x-1">
          <UserIcon className="w-4 h-4" />
          <span className="">
            {college.size ? college.size.toLocaleString() : 'Unknown'}
          </span>
        </div>
        <h1 className="">
          Average SAT: &nbsp;
          {college.sat_score}
        </h1>
      </div>
      <div className="flex w-full justify-between mb-2 text-xs font-semibold italic text-gray-500">
        {college.out_of_state_tuition ? (
          <>
            <span>
              $
              {college.in_state_tuition
                ? college.in_state_tuition.toLocaleString()
                : 'Unknown'}{' '}
              In-State
            </span>
            <span>
              $
              {college.out_of_state_tuition
                ? college.out_of_state_tuition.toLocaleString()
                : 'Unknown'}{' '}
              Out-State
            </span>
          </>
        ) : (
          <span>
            $
            {college.in_state_tuition
              ? college.in_state_tuition.toLocaleString()
              : 'Unknown'}{' '}
            Tuition
          </span>
        )}
      </div>
      <span className="font-bold text-primary text-sm">
        {college.admission_rate
          ? Math.round(college.admission_rate * 100) + '% '
          : 'Unknown '}
        Admitted
      </span>
      <progress
        className="progress progress-primary w-full"
        value={college.admission_rate}
        max="1"
      ></progress>
    </div>
  );
};

const CollegeCardSkel = () => {
  return (
    <div className="bg-base-300 p-6 rounded-lg mx-auto animate-pulse opacity-25">
      <div className="h-40 rounded bg-stone-400 w-full mb-6" />
      <div className="h-2 rounded-full bg-stone-400 w-10 mb-2" />
      <div className="h-5 rounded-full bg-stone-400 w-full mb-2" />
      <div className="flex w-full justify-between font-semibold text-sm mb-1">
        <div className="h-3 rounded-full bg-stone-400 w-10 mb-2" />
        <div className="h-3 rounded-full bg-stone-400 w-10 mb-2" />
      </div>
      <div className="flex w-full justify-between mb-2 text-xs font-semibold italic text-gray-500">
        <div className="h-3 rounded-full bg-stone-400 w-1/3 mb-2" />
        <div className="h-3 rounded-full bg-stone-400 w-1/3" />
      </div>
      <div className="h-3 rounded-full bg-stone-400 w-1/2 mb-2" />
      <div className="h-2 rounded-full bg-stone-400 w-full" />
    </div>
  );
};

export {CollegeCard, CollegeCardSkel};
