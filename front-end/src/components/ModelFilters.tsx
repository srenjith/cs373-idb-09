import {Fragment, useState} from 'react';
import {Combobox, Listbox, Popover, Transition} from '@headlessui/react';
import {
  CheckIcon,
  ChevronUpDownIcon,
  FunnelIcon,
} from '@heroicons/react/24/solid';
import {RangeSlider} from './RangeSlider';

type ModelDropdownProps = {
  chosenVal: string;
  setChosenVal: any;
  placeholder: string;
  defVal: string;
  vals: string[];
};

export type RangeFilterProps = {
  displayName: string;
  filterName: string;
  rangeMin: number;
  rangeMax: number;
  minVal: number;
  maxVal: number;
  setMinVal: any;
  setMaxVal: any;
  symbol?: string;
  steps?: number;
  pictorial?: true;
};

type RangeFiltersProps = {
  filters: RangeFilterProps[];
  handleSliderChange: any;
  applyFilters: () => void;
};

export const ModelDropdown = ({
  chosenVal,
  setChosenVal,
  placeholder,
  defVal,
  vals,
}: ModelDropdownProps) => {
  return (
    <Listbox value={chosenVal} onChange={setChosenVal}>
      <div className="relative mr-4 mb-4">
        <Listbox.Button className="input text-left relative w-[25ch] rounded-lg cursor-pointer text-sm">
          <span
            className={`block ${chosenVal === placeholder && 'text-gray-400'}`}
          >
            {chosenVal}
          </span>
          <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
            <ChevronUpDownIcon
              className="h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
          </span>
        </Listbox.Button>
        <Transition
          as={Fragment}
          enter="transition ease-in duration-100"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Listbox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg outline-none sm:text-sm">
            {vals.map((val: string, index: number) => (
              <Listbox.Option
                key={index}
                className={({active}) =>
                  `relative cursor-default select-none py-2 pl-10 pr-4 ${
                    (active && val !== placeholder) ||
                    (chosenVal === placeholder && val === defVal)
                      ? 'bg-amber-100 text-amber-900'
                      : val === placeholder
                      ? 'text-gray-400'
                      : 'text-gray-900'
                  }`
                }
                value={val}
                disabled={val === placeholder && placeholder !== defVal}
              >
                {({selected}) => (
                  <>
                    <span
                      className={`block truncate ${
                        (selected && val !== placeholder) ||
                        (chosenVal === placeholder && val === defVal)
                          ? 'font-medium'
                          : 'font-normal'
                      }`}
                    >
                      {val}
                    </span>
                    {((selected && val !== placeholder) ||
                      (chosenVal === placeholder && val === defVal)) && (
                      <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                      </span>
                    )}
                  </>
                )}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
      </div>
    </Listbox>
  );
};

export const ModelDropdownAuto = ({
  chosenVal: value,
  setChosenVal: setValue,
  placeholder,
  defVal,
  vals,
}: ModelDropdownProps) => {
  const [query, setQuery] = useState('');

  const filteredOptions =
    query === ''
      ? vals
      : vals.filter(val =>
          val
            .toLowerCase()
            .replace(/\s+/g, '')
            .includes(query.toLowerCase().replace(/\s+/g, ''))
        );

  const titleCase = (str: string) => {
    return str
      .toLowerCase()
      .split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
      .join(' ');
  };
  return (
    <Combobox value={value} onChange={setValue}>
      <div className="relative mr-4 mb-4">
        <Combobox.Input
          className="input text-left relative w-[25ch] rounded-lg cursor-pointer text-sm"
          displayValue={() => value}
          onChange={event => setQuery(event.target.value)}
          placeholder={placeholder}
        />
        <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
          <ChevronUpDownIcon
            className="h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
        </Combobox.Button>
        <Transition
          as={Fragment}
          enter="transition ease-in duration-100"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          afterLeave={() => setQuery('')}
        >
          <Combobox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg outline-none sm:text-sm">
            {filteredOptions.length === 0 && query !== '' ? (
              <div className="relative cursor-default select-none py-2 px-4 text-gray-700">
                Nothing found.
              </div>
            ) : (
              filteredOptions.map((val: string, index: number) => (
                <Combobox.Option
                  key={index}
                  className={({active}) =>
                    `relative cursor-default select-none py-2 pl-10 pr-4 ${
                      active && val !== placeholder
                        ? 'bg-amber-100 text-amber-900'
                        : val === placeholder
                        ? 'text-gray-400'
                        : 'text-gray-900'
                    }`
                  }
                  value={titleCase(val)}
                  disabled={val === placeholder}
                >
                  {({selected}) => (
                    <>
                      <span
                        className={`block truncate ${
                          (selected && val !== placeholder) ||
                          (value === placeholder && val === defVal)
                            ? 'font-medium'
                            : 'font-normal'
                        }`}
                      >
                        {titleCase(val)}
                      </span>
                      {((selected && val !== placeholder) ||
                        (value === placeholder && val === defVal)) && (
                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                          <CheckIcon className="h-5 w-5" aria-hidden="true" />
                        </span>
                      )}
                    </>
                  )}
                </Combobox.Option>
              ))
            )}
          </Combobox.Options>
        </Transition>
      </div>
    </Combobox>
  );
};

export const RangeFilters = ({
  filters,
  handleSliderChange,
  applyFilters,
}: RangeFiltersProps) => {
  const handleEnter = (e: any) => {
    if (e.key === 'Enter') {
      applyFilters();
    }
  };
  return (
    <Popover className="relative mr-4 mb-4">
      {({open}) => (
        <>
          <Popover.Button
            className={`
      ${open ? '' : 'text-opacity-90'}
      group inline-flex items-center rounded-lg bg-white input font-medium text-sm hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75`}
          >
            <span>More Filters</span>
            <FunnelIcon
              className={`${open ? '' : 'text-opacity-70'}
        ml-2 h-5 w-5 transition duration-150 ease-in-out group-hover:text-opacity-80`}
              aria-hidden="true"
            />
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute z-10 mt-1">
              <div className=" bg-white p-4 rounded-lg w-[90vw] md:w-[24rem] shadow-md">
                {filters.map((filter, index) => (
                  <div key={index} className="mb-2">
                    <label className="label">
                      <span className="label-text font-semibold">
                        {filter.displayName}
                      </span>
                    </label>
                    <div className="flex items-center space-x-6">
                      <div className="flex items-center">
                        {/* <span>{filter.symbol !== '%' ? filter.symbol : ' '}</span> */}
                        {!filter.pictorial ? (
                          <input
                            type="number"
                            value={filter.minVal}
                            onChange={filter.setMinVal}
                            className="text-sm  text-center border-2 rounded-lg p-1 w-16"
                            min={filter.rangeMin}
                            max={filter.maxVal}
                            onKeyUp={handleEnter}
                          />
                        ) : (
                          <span className="text-sm  text-center border-2 rounded-lg p-1 w-16">
                            {filter.symbol
                              ? filter.symbol.repeat(filter.minVal)
                              : ' '}
                          </span>
                        )}
                        {/* <span>{filter.symbol === '%' ? filter.symbol : ' '}</span> */}
                      </div>
                      <RangeSlider
                        min={filter.rangeMin}
                        max={filter.rangeMax}
                        value={[filter.minVal, filter.maxVal]}
                        onChange={(e: Event, newValue: number | number[]) =>
                          handleSliderChange(e, newValue, filter.filterName)
                        }
                        step={filter.steps}
                        disableSwap
                      />
                      <div className="flex items-center">
                        {/* <span>{filter.symbol !== '%' ? filter.symbol : ' '}</span> */}
                        {!filter.pictorial ? (
                          <input
                            type="number"
                            value={filter.maxVal}
                            onChange={filter.setMaxVal}
                            className="text-sm text-center border-2 rounded-lg p-1  w-16"
                            min={filter.minVal}
                            max={filter.rangeMax}
                            onKeyUp={handleEnter}
                          />
                        ) : (
                          <span className="text-sm  text-center border-2 rounded-lg p-1 w-16">
                            {filter.symbol
                              ? filter.symbol.repeat(filter.maxVal)
                              : ' '}
                          </span>
                        )}
                        {/* <span>{filter.symbol === '%' ? filter.symbol : ' '}</span> */}
                      </div>
                    </div>
                  </div>
                ))}
                <div onClick={applyFilters}>
                  <Popover.Button className="btn btn-primary w-full mt-4">
                    Apply
                  </Popover.Button>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};
