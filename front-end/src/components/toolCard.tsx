export type Tool = {
  name: string;
  description: string;
  image: string;
  link: string;
};

export const ToolCard = ({tool, type}: {tool: Tool; type: string}) => {
  return (
    <a
      href={tool.link}
      target="_blank"
      rel="noopener noreferrer"
      className="block lg:w-1/5 md:w-1/2 p-4 w-full rounded-lg hover:bg-base-300 hover:font-semibold hover:scale-105 transition-all"
    >
      <img
        alt={tool.name}
        className="aspect-square object-cover object-center rounded-lg w-full block"
        src={tool.image}
      />
      <div className="mt-4">
        <h3 className="text-xs tracking-widest title-font mb-1">{type}</h3>
        <h2 className="title-font text-lg font-medium">{tool.name}</h2>
        <p className="mt-1">{tool.description}</p>
      </div>
    </a>
  );
};
