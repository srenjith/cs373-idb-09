import {TeamMember} from '../static/AboutInfo';

export const MemberCard = ({member}: {member: TeamMember}) => {
  return (
    <div className="xl:w-1/5 md:w-1/3 p-4">
      <div className="bg-base-100 p-6 rounded-lg ">
        <img
          className=" aspect-[7/6] bg-accent-content rounded-lg w-full object-cover object-center mb-6"
          src={member.image}
          alt="content"
        />
        <h3 className="tracking-widest text-accent-content text-xs font-bold title-font">
          @{member.username}
        </h3>
        <h2 className="text-base font-semibold title-font mb-2">
          {member.name}
        </h2>
        <div className=" mb-4 space-y-1">
          <h4 className="text-xs font-bold py-1 px-2 border-[1px] border-primary text-primary rounded-full w-max">
            {member.commits} Commits
          </h4>
          <div className="flex space-x-1">
            <h4 className="text-xs font-bold py-1 px-2 border-[1px] border-error text-error rounded-full w-max">
              {member.issues} Issues
            </h4>
            <h4 className="text-xs font-bold py-1 px-2 border-[1px] border-success  text-success rounded-full w-max">
              {member.unitTests} Unit Tests
            </h4>
          </div>
        </div>
        <p className="leading-relaxed text-sm">{member.bio}</p>
      </div>
    </div>
  );
};
