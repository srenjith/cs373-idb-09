type EmbeddedMapProps = {
  locationName: string;
  city?: string;
  state?: string;
};

const EmbeddedMap = ({locationName, city, state}: EmbeddedMapProps) => {
  const location = locationName.replace('&', 'and');
  const APIURL = `https://www.google.com/maps/embed/v1/place?key=AIzaSyB5f_1DwqTNzelmdpqWYMvHgQ_4iI4zEHU&q=${location},${city}+${state}`;

  return (
    <iframe
      className="rounded-lg w-full h-full animate-fade-in-up"
      src={APIURL}
    ></iframe>
  );
};

export default EmbeddedMap;
