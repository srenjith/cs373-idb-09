import dayjs from 'dayjs';
import {Event} from '../api/API';
import DefaultEvent from '../assets/college_default.png';
import {CalendarIcon, GlobeAmericasIcon} from '@heroicons/react/24/solid';
import {getHighlightedText} from '../static/SearchSortFilterData';

const EventCard = ({event, search}: {event: Event; search?: string}) => {
  const formattedTime = dayjs(event.start_date).format('dddd, MMMM D');

  return (
    <div className="bg-base-100 p-6 rounded-lg mx-auto hover:scale-95 transition-all animate-fade-in-up">
      <img
        className="h-40 rounded w-full object-cover object-center mb-6"
        src={event.image}
        alt="content"
        onError={err => {
          (err.target as HTMLImageElement).src = DefaultEvent;
          (err.target as HTMLImageElement).onerror = null;
        }}
      />
      <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
        {getHighlightedText(search, event.city)},{' '}
        {getHighlightedText(search, event.state)}
      </h3>
      <h2 className="text-lg text-gray-900 font-medium title-font mb-1 truncate">
        {getHighlightedText(search, event.name)}
      </h2>
      <div className="flex items-center space-x-2 font-semibold text-gray-500 text-sm mb-1">
        <CalendarIcon className="w-5 h-5" />
        <span className="">{getHighlightedText(search, formattedTime)}</span>
      </div>
      <div className="flex items-center space-x-2 font-semibold text-gray-500 text-sm mb-2">
        <GlobeAmericasIcon className="w-5 h-5" />
        <span className="truncate">
          {getHighlightedText(search, event.venue_name)}
        </span>
      </div>
      <h1 className="text-right ml-auto w-max font-medium text-base-100 bg-primary px-2 py-1 rounded-lg">
        {event.min_price.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        })}{' '}
        -{' '}
        {event.max_price.toLocaleString('en-US', {
          style: 'currency',
          currency: 'USD',
        })}
      </h1>
    </div>
  );
};

const EventCardSkel = () => {
  return (
    <div className="bg-base-300 p-6 rounded-lg mx-auto animate-pulse opacity-25">
      <div className="h-40 rounded bg-stone-400 w-full mb-6" />
      <div className="h-2 rounded-full bg-stone-400 w-1/2 mb-2" />
      <div className="h-5 rounded-full bg-stone-400 w-full mb-2" />
      <div className="h-3 rounded-full bg-stone-400 w-3/4 mb-2" />
      <div className="h-3 rounded-full bg-stone-400 w-1/2 mb-2" />
      <div className="h-7 rounded-lg  bg-stone-400 w-1/3 ml-auto  " />
    </div>
  );
};

export {EventCard, EventCardSkel};
