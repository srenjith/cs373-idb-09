import {
  ChevronDoubleLeftIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ChevronDoubleRightIcon,
} from '@heroicons/react/24/solid';

type PaginationBarProps = {
  page: number;
  totalPages: number;
  totalItems: number;
  setPage: (page: number) => void;
};

const PaginationBar = ({
  page,
  totalPages,
  setPage,
  totalItems: totalResults,
}: PaginationBarProps) => {
  return (
    // vstack div
    <div className="flex flex-col space-y-4 justify-center items-center">
      <div className="w-full flex justify-center items-center space-x-4">
        {page > 1 && (
          <>
            <div className="btn-group">
              <button
                className="btn active:btn-active"
                onClick={() => setPage(1)}
              >
                <ChevronDoubleLeftIcon className="w-6 h-6" />
              </button>
            </div>
            <div className="btn-group">
              <button
                className="btn active:btn-active"
                onClick={() => setPage(page - 1)}
              >
                <ChevronLeftIcon className="w-6 h-6" />
              </button>
            </div>
          </>
        )}
        <div className="btn-group">
          {page - 2 > 0 && (
            <button
              className="btn active:btn-active"
              onClick={() => setPage(page - 2)}
            >
              {page - 2}
            </button>
          )}
          {page - 1 > 0 && (
            <button
              className="btn active:btn-active"
              onClick={() => setPage(page - 1)}
            >
              {page - 1}
            </button>
          )}
          <button className="btn btn-active">{page}</button>
          {page + 1 <= totalPages && (
            <button
              className="btn active:btn-active"
              onClick={() => setPage(page + 1)}
            >
              {page + 1}
            </button>
          )}
          {page + 2 <= totalPages && (
            <button
              className="btn active:btn-active"
              onClick={() => setPage(page + 2)}
            >
              {page + 2}
            </button>
          )}
        </div>
        {page < totalPages && (
          <>
            <div className="btn-group">
              <button
                className="btn active:btn-active"
                onClick={() => setPage(page + 1)}
              >
                <ChevronRightIcon className="w-6 h-6" />
              </button>
            </div>
            <div className="btn-group">
              <button
                className="btn active:btn-active"
                onClick={() => setPage(totalPages)}
              >
                <ChevronDoubleRightIcon className="w-6 h-6" />
              </button>
            </div>
          </>
        )}
      </div>
      <div className="text-gray-800">
        Page {page} of {totalPages} ({totalResults} results)
      </div>
    </div>
  );
};

export default PaginationBar;
