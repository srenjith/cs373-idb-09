import {MagnifyingGlassIcon} from '@heroicons/react/24/solid';
import {ChangeEventHandler} from 'react';

const SearchBar = ({
  value,
  onChange,
  onEnter,
  placeholder,
  nav,
  standalone,
}: {
  value: string | undefined;
  onChange: any;
  onEnter?: any;
  placeholder: string;
  nav?: boolean;
  standalone?: boolean;
}) => {
  const onKeyUp = (e: any) => {
    if (e.key === 'Enter') {
      onEnter();
    }
  };
  return (
    <div
      className={`flex ${
        !nav && `mr-4 mb-4 w-full ${standalone ? 'max-w-xl' : 'max-w-sm'}`
      }`}
    >
      <input
        type="text"
        placeholder={placeholder}
        className={`input ${
          nav && 'input-sm'
        } w-full rounded-l-lg rounded-r-none focus:outline-none focus:ring-0 text-sm`}
        value={value}
        onChange={onChange}
        onKeyUp={onKeyUp}
      />
      <button
        className={`btn ${
          nav && 'btn-sm'
        } btn-accent rounded-r-lg rounded-l-none`}
        onClick={onEnter}
      >
        <MagnifyingGlassIcon className={`${nav ? 'w-4 h-4' : 'w-6 h-6'}`} />
      </button>
    </div>
  );
};

export default SearchBar;
