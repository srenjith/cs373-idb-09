import {Fragment} from 'react';
import {Dialog, Transition} from '@headlessui/react';

const InstanceModal = ({
  instance,
  isOpen,
  closeModal,
}: {
  instance: any;
  isOpen: boolean;
  closeModal: any;
}) => {
  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-50" onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-[35%]" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="grid place-items-center h-full p-4">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-4xl overflow-scroll max-h-[36rem] p-8 rounded-lg bg-base-100 shadow-xl transition-all">
                {instance}
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default InstanceModal;
