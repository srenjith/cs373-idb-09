import {useEffect, useState} from 'react';
import EmbeddedMap from '../components/map';
import {EventAPI, Event, Restaurant, College} from '../api/API';
import dayjs from 'dayjs';
import {RestaurantCard} from '../components/RestaurantCard';
import DefaultEvent from '../assets/college_default.png';
import {CollegeCard} from '../components/CollegeCard';

const EventInstance = ({id}: {id: number}) => {
  const [event, setEvent] = useState<Event>();
  const [nearbyRestaurants, setNearbyRestaurants] = useState<Restaurant[]>([]);
  const [nearbyColleges, setNearbyColleges] = useState<College[]>([]);

  const getEvent = async () => {
    await EventAPI.getEvent(id).then(res => {
      setEvent(res);
    });
  };

  useEffect(() => {
    getEvent();
    EventAPI.getNearbyRestaurants(id).then(res => {
      const restaurants = res.map(x => x.restaurant);
      setNearbyRestaurants(restaurants);
    });
    EventAPI.getNearbyColleges(id).then(res => {
      const colleges = res.map(x => x.college);
      setNearbyColleges(colleges);
    });
  }, [id]);

  return !event ? (
    <div></div>
  ) : (
    <>
      <div className="md:flex h-3/4 flex-row-reverse">
        <div className="md:w-1/2 md:ml-8">
          <img
            className="w-full h-full rounded-lg aspect-square m-auto object-cover object-center animate-fade-in-up"
            src={event.image}
            alt="event"
            onError={err => {
              (err.target as HTMLImageElement).src = DefaultEvent;
              (err.target as HTMLImageElement).onerror = null;
            }}
          />
        </div>
        <div className="md:w-1/2 md:mt-0 mt-4 h-full space-y-4 overflow-scroll">
          <h1 className="font-semibold text-xl">{event.name}</h1>
          <p className="text-sm leading-loose">
            {event.name} is located in {event.city}, {event.state}. It will take
            place on {dayjs(event.start_date).format('dddd, MMMM D')}. The
            minimum ticket price is ${event.min_price}.
          </p>
          <div>
            <a href={event.url} target="_blank" rel="noopener noreferrer">
              <button className="btn btn-primary btn-sm mr-2">
                Buy Tickets
              </button>
            </a>
          </div>
          <EmbeddedMap locationName={event.venue_name} />
        </div>
      </div>
      <h1 className="text-xl mt-6 font-medium"> Restaurants Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyRestaurants.map((restaurant, index) => (
          <a key={index} href={`/restaurants/${restaurant.id}`} className="">
            <RestaurantCard restaurant={restaurant} />
          </a>
        ))}
      </div>
      <h1 className="text-xl mt-6 font-medium"> Colleges Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyColleges.map((college, index) => (
          <a key={index} href={`/colleges/${college.id}`} className="">
            <CollegeCard college={college} />
          </a>
        ))}
      </div>
    </>
  );
};

export default EventInstance;
