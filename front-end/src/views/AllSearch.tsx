import React, {ChangeEvent, useState} from 'react';
import SearchBar from '../components/SearchBar';

const AllSearch = () => {
  const [searchTerm, setSearchTerm] = useState('');

  const makeSearch = () => {
    if (searchTerm.length > 0) {
      window.location.href = `/search/${searchTerm}`;
    }
  };

  return (
    <section className="min-h-[90vh] grid place-items-center animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-6">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Search
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded mb-2"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Browse through colleges, restaurants, and events all at once. With
            just a few clicks, you can find the perfect college, restaurant, and
            event that fits your preferences and schedule.
          </p>
          <SearchBar
            value={searchTerm}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setSearchTerm(e.target.value)
            }
            placeholder="Search"
            onEnter={makeSearch}
            standalone
          />
        </div>
      </div>
    </section>
  );
};

export default AllSearch;
