import {useEffect, useState} from 'react';
import {College, CollegeAPI, Restaurant, Event} from '../api/API';
import EmbeddedMap from '../components/map';
import {RestaurantCard} from '../components/RestaurantCard';
import DefaultCollege from '../assets/college_default.png';
import {EventCard} from '../components/EventCard';

const CollegeInstance = ({id}: {id: number}) => {
  const [college, setCollege] = useState<College>();
  const [nearbyRestaurants, setNearbyRestaurants] = useState<Restaurant[]>([]);
  const [nearbyEvents, setNearbyEvents] = useState<Event[]>([]);

  const getCollege = async () => {
    await CollegeAPI.getCollege(id).then(res => {
      setCollege(res);
    });
  };

  const getNearbyRestaurants = async () => {
    await CollegeAPI.getNearbyRestaurants(id).then(res => {
      const restaurants = res.map(x => x.restaurant);
      setNearbyRestaurants(restaurants);
    });
  };

  const getNearbyEvents = async () => {
    await CollegeAPI.getNearbyEvents(id).then(res => {
      const events = res.map(x => x.event);
      setNearbyEvents(events);
    });
  };

  useEffect(() => {
    getCollege();
    getNearbyRestaurants();
    getNearbyEvents();
  }, [id]);

  return !college ? (
    <div></div>
  ) : (
    <>
      <div className="md:flex h-3/4 flex-row-reverse">
        <div className="md:w-1/2 md:ml-8">
          <img
            className="w-full h-full rounded-lg aspect-square m-auto object-cover object-center animate-fade-in-up"
            src={college.image}
            alt="College"
            onError={err => {
              (err.target as HTMLImageElement).src = DefaultCollege;
              (err.target as HTMLImageElement).onerror = null;
            }}
          />
        </div>
        <div className="md:w-1/2 md:mt-0 mt-4 md:grid grid-rows-[auto 1fr] gap-4 overflow-scroll">
          <div className="space-y-4">
            <h1 className="font-semibold text-xl">{college.name}</h1>
            <p className="text-sm leading-loose">
              {college.name} is located in {college.city}, {college.state} and
              has a student body of {college.size} students. The average SAT
              score is {college.sat_score} and the admission rate is{' '}
              {Math.round(college.admission_rate * 100)}%
            </p>
          </div>
          <EmbeddedMap
            locationName={college.name}
            city={college.city}
            state={college.state}
          />
        </div>
      </div>
      <h1 className="text-xl mt-6 font-medium"> Restaurants Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyRestaurants.map((restaurant, index) => (
          <a key={index} href={`/restaurants/${restaurant.id}`} className="">
            <RestaurantCard restaurant={restaurant} />
          </a>
        ))}
      </div>
      <h1 className="text-xl mt-6 font-medium"> Events Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyEvents.map((event, index) => (
          <a key={index} href={`/events/${event.id}`} className="">
            <EventCard event={event} />
          </a>
        ))}
      </div>
    </>
  );
};

export default CollegeInstance;
