import {ChangeEvent, useEffect, useState} from 'react';
import {EventCard, EventCardSkel} from '../components/EventCard';
import PaginationBar from '../components/paginationBar';
import InstanceModal from '../components/InstanceModal';
import EventInstance from './EventInstanceScreen';
import {useParams} from 'react-router-dom';
import {EventAPI, Event} from '../api/API';
import {EventSortable} from '../api/event';
import {
  ModelDropdown,
  ModelDropdownAuto,
  RangeFilterProps,
  RangeFilters,
} from '../components/ModelFilters';
import {Cities, EventSort, StatesFull} from '../static/SearchSortFilterData';
import Datepicker from 'react-tailwindcss-datepicker';
import {DateValueType} from 'react-tailwindcss-datepicker/dist/types';
import SearchBar from '../components/SearchBar';

export const staticEvents: Event[] = [];

const EventScreen = () => {
  const emptyIter = Array.from(Array(4).keys());
  const [events, setEvents] = useState<Event[]>([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  const paramID = parseInt(useParams<{id: string}>().id || '-1');
  const [id, setId] = useState(paramID);
  const [isOpen, setIsOpen] = useState(id !== -1);

  const [queryFailed, setQueryFailed] = useState(false);

  const [filters, setFilters] = useState({
    search: '',
    sortValue: 'Sort By',
    state: 'Select State',
    city: '',
    venue: '',
    dateRange: {startDate: null, endDate: null},
    minPrice: 0,
    maxPrice: 6000,
  });

  const [isReset, setIsReset] = useState(false);

  const rangeFilters: RangeFilterProps[] = [
    {
      displayName: 'Price',
      filterName: 'Price',
      rangeMin: 0,
      rangeMax: 6000,
      minVal: filters.minPrice,
      maxVal: filters.maxPrice,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minPrice');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxPrice');
      },
      symbol: '$',
    },
  ];

  const applySearchFilters = () => {
    setEvents([]);
    getEvents();
  };

  useEffect(() => {
    getEvents();
  }, []);

  useEffect(() => {
    applySearchFilters();
  }, [
    page,
    filters.search,
    filters.sortValue,
    filters.city,
    filters.state,
    filters.dateRange,
    isReset,
  ]);

  const handleFilterChange = (e: any, filter: string) => {
    if (e === null) return;
    let newValue =
      typeof e === 'string' || e.hasOwnProperty('startDate')
        ? e
        : e.target
        ? (e.target as HTMLInputElement).value
        : '';
    const newFilters =
      filter === 'state'
        ? {...filters, state: newValue, city: ''}
        : {...filters, [filter]: newValue};
    setFilters(newFilters);
  };

  const handleSliderChange = (
    e: ChangeEvent,
    newValue: number[],
    filter: string
  ) => {
    const newFilters = {
      ...filters,
      ['min' + filter]: newValue[0],
      ['max' + filter]: newValue[1],
    };
    setFilters(newFilters);
  };

  const reset = () => {
    setFilters({
      search: '',
      sortValue: 'Sort By',
      state: 'Select State',
      city: '',
      venue: '',
      dateRange: {startDate: null, endDate: null},
      minPrice: 0,
      maxPrice: 6000,
    });

    setIsReset(!isReset);
  };

  function closeModal() {
    setIsOpen(false);
  }

  function openModal(collegeId: number) {
    setId(collegeId);
    setIsOpen(true);
  }

  const getEvents = async () => {
    let sort =
      filters.sortValue === 'Sort By'
        ? ''
        : filters.sortValue
            .toLowerCase()
            .replaceAll('-', '_')
            .replaceAll(' ', '_');
    sort = sort === 'date' ? 'start_date' : sort;
    sort = sort === 'venue' ? 'venue_name' : sort;
    await EventAPI.getEvents({
      page: page,
      search: filters.search,
      sort: sort as EventSortable,
      exactFilters: {
        state: filters.state === 'Select State' ? '' : filters.state,
        city: filters.city,
      },
      rangeFilters: {
        min: {
          start_date: filters.dateRange.startDate || '',
          min_price: filters.minPrice,
        },
        max: {
          start_date: filters.dateRange.endDate || '',
          max_price: filters.maxPrice,
        },
      },
    }).then(res => {
      const fetchedEvents = res.results;
      const numPages = res.metadata.total_pages;
      setEvents(fetchedEvents);
      setTotalPages(numPages);
      setTotalItems(res.metadata.total_items);
      setQueryFailed(fetchedEvents.length === 0);
    });
  };

  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Events
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Browse our Events page and discover the best ways to spend your free
            time in the area. From music and art to food and culture, there's
            always something new and exciting to experience. Don't miss out on
            the fun!
          </p>
          <div className="flex flex-wrap w-full">
            <SearchBar
              value={filters.search}
              onChange={(e: ChangeEvent) => handleFilterChange(e, 'search')}
              placeholder={'Search Events'}
            />
            <ModelDropdown
              chosenVal={filters.sortValue}
              setChosenVal={(e: ChangeEvent) =>
                handleFilterChange(e, 'sortValue')
              }
              placeholder="Sort By"
              defVal="SAT Score"
              vals={EventSort}
            />
            <ModelDropdown
              chosenVal={filters.state}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'state')}
              placeholder="Select State"
              defVal="Select State"
              vals={StatesFull}
            />
            <ModelDropdownAuto
              chosenVal={filters.city}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'city')}
              placeholder="Select City"
              defVal="Select City"
              vals={Cities(filters.state)}
            />
            <Datepicker
              value={filters.dateRange}
              containerClassName="w-full max-w-xs mr-4 mb-4"
              inputClassName="input border-0 font-normal"
              separator={'to'}
              onChange={(value: DateValueType) =>
                handleFilterChange(value, 'dateRange')
              }
              showShortcuts={true}
              primaryColor={'yellow'}
              useRange={false}
            />
            <RangeFilters
              filters={rangeFilters}
              handleSliderChange={handleSliderChange}
              applyFilters={applySearchFilters}
            />
            <button className="btn" onClick={reset}>
              {' '}
              Reset Search and Filters
            </button>
          </div>
        </div>
        <div className="md:flex flex-wrap -m-4">
          {events.length > 0 ? (
            events.map((event, index) => (
              <div
                key={index}
                onClick={() => openModal(event.id)}
                className="xl:w-1/4 md:w-1/2 p-4 block cursor-pointer"
              >
                <EventCard event={event} search={filters.search} />
              </div>
            ))
          ) : !queryFailed ? (
            emptyIter.map((_, index) => (
              <div key={index} className="xl:w-1/4 md:w-1/2 p-4 block">
                <EventCardSkel />
              </div>
            ))
          ) : (
            <div className="w-full text-center">No results found</div>
          )}
        </div>
        {events.length > 0 && (
          <div className="pt-14">
            <PaginationBar
              page={page}
              setPage={setPage}
              totalPages={totalPages}
              totalItems={totalItems}
            />
          </div>
        )}
      </div>
      <InstanceModal
        instance={<EventInstance id={id} />}
        isOpen={isOpen}
        closeModal={closeModal}
      />
    </section>
  );
};

export default EventScreen;
