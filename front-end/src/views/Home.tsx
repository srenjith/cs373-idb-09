import Hero from '../components/Hero';
import aboutHomeImage from '../assets/aboutHomeImage.svg';
import {
  AcademicCapIcon,
  FireIcon,
  TicketIcon,
  ArrowSmallRightIcon,
} from '@heroicons/react/24/outline';

const Home = () => {
  const navlinks = [
    {
      name: 'Colleges',
      path: '/colleges',
      description:
        'Learn more about colleges across the country that you may be interested in.',
      icon: <AcademicCapIcon className="w-6 h-6" />,
    },
    {
      name: 'Restaurants',
      path: '/restaurants',
      description:
        'Find the best places to eat near you when planning out your day.',
      icon: <FireIcon className="w-6 h-6" />,
    },
    {
      name: 'Events',
      path: '/events',
      description:
        'Discover events happening near you and have a blast with your friends.',
      icon: <TicketIcon className="w-6 h-6" />,
    },
  ];
  return (
    <div className="animate-fade-in-up">
      <Hero />
      <section id="explore" className="hero min-h-screen">
        <div className="px-6">
          <div className="flex flex-col text-center w-full ">
            <img src={aboutHomeImage} className="w-1/3 md:w-1/6 mx-auto" />
            <h2 className="text-xs text-accent tracking-widest font-medium title-font mb-2">
              DAY PLAN
            </h2>
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2">
              Explore your area
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-base mb-2">
              Campus Eats & Treats can help you and your decide you next weekend
              plans as you explore info about various universities, events
              nearby and great spots to eat while there
            </p>
            <a
              href="/about"
              className="flex justify-center font-medium border-b-2 hover:border-base-content transition-all w-max mx-auto"
            >
              <span>Learn more</span>{' '}
              <ArrowSmallRightIcon className="w-6 h-6" />
            </a>
          </div>
          <div className="flex flex-wrap mt-4 mb-12 md:mb-0">
            {navlinks.map((link, index) => (
              <a
                href={link.path}
                key={index}
                className="p-4 md:w-1/3 hover:scale-105 transition-all"
              >
                <div className="flex rounded-lg h-full bg-base-100 p-8 flex-col">
                  <div className="flex items-center mb-3">
                    <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-primary text-base-100 flex-shrink-0">
                      {link.icon}
                    </div>
                    <h2 className="text-lg title-font font-medium">
                      {link.name}
                    </h2>
                  </div>
                  <div className="flex-grow">
                    <p className="leading-relaxed text-base">
                      {link.description}
                    </p>
                  </div>
                </div>
              </a>
            ))}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
