import {useEffect, useState} from 'react';
import {College, Event, Restaurant, RestaurantAPI} from '../api/API';
import EmbeddedMap from '../components/map';
import {EventCard} from '../components/EventCard';
import DefaultRestaurant from '../assets/restaurant_default.png';
import {CollegeCard} from '../components/CollegeCard';

const RestaurantInstance = ({id}: {id: number}) => {
  const [restaurant, setRestaurant] = useState<Restaurant>();
  const [nearbyEvents, setNearbyEvents] = useState<Event[]>([]);
  const [nearbyColleges, setNearbyColleges] = useState<College[]>([]);

  const getRestaurant = async () => {
    await RestaurantAPI.getRestaurant(id).then(res => {
      setRestaurant(res);
    });
  };

  useEffect(() => {
    getRestaurant();

    RestaurantAPI.getNearbyEvents(id).then(res => {
      const events = res.map(x => x.event);
      setNearbyEvents(events as unknown as Event[]);
    });

    RestaurantAPI.getNearbyColleges(id).then(res => {
      const colleges = res.map(x => x.college);
      setNearbyColleges(colleges);
    });
  }, [id]);

  return !restaurant ? (
    <div></div>
  ) : (
    <>
      <div className="md:flex h-3/4 flex-row-reverse">
        <div className="md:w-1/2 md:ml-8">
          <img
            className="w-full h-full rounded-lg aspect-square m-auto object-cover object-center animate-fade-in-up"
            src={restaurant.image}
            alt="restaurant"
            onError={err => {
              (err.target as HTMLImageElement).src = DefaultRestaurant;
              (err.target as HTMLImageElement).onerror = null;
            }}
          />
        </div>
        <div className="md:w-1/2 md:mt-0 mt-4  md:grid grid-rows-[auto 1fr] gap-4 overflow-scroll">
          <div className="space-y-4">
            <h1 className="font-semibold text-xl">{restaurant.name}</h1>
            <p className="text-sm leading-loose">
              {restaurant.name} is located in {restaurant.city},{' '}
              {restaurant.state} and has a rating of {restaurant.rating} stars.
              The cuisine is {restaurant.category}. The price range is around{' '}
              {'$'.repeat(restaurant.price)}.
            </p>
            <div>
              <a
                href={restaurant.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                <button className="btn btn-primary btn-sm mr-2">Website</button>
              </a>
              <a href={'tel:' + restaurant.phone}>
                <button className="btn btn-secondary btn-sm mr-2">Call</button>
              </a>
            </div>
          </div>
          <EmbeddedMap
            locationName={restaurant.name}
            city={restaurant.city}
            state={restaurant.state}
          />
        </div>
      </div>
      <h1 className="text-xl mt-6 font-medium"> Events Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyEvents.map((event, index) => (
          <a key={index} href={`/events/${event.id}`}>
            <EventCard event={event} />
          </a>
        ))}
      </div>
      <h1 className="text-xl mt-6 font-medium"> Colleges Nearby</h1>
      <div className="flex overflow-scroll">
        {nearbyColleges.map((college, index) => (
          <a key={index} href={`/college/${college.id}`}>
            <CollegeCard college={college} />
          </a>
        ))}
      </div>
    </>
  );
};

export default RestaurantInstance;
