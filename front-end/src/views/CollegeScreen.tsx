import {ChangeEvent, useEffect, useState} from 'react';
import {CollegeCard, CollegeCardSkel} from '../components/CollegeCard';
import PaginationBar from '../components/paginationBar';
import InstanceModal from '../components/InstanceModal';
import CollegeInstance from './CollegeInstanceScreen';
import {useParams} from 'react-router-dom';
import {College, CollegeAPI} from '../api/API';
import {CollegeSort, StatesAbbr, Cities} from '../static/SearchSortFilterData';
import {
  ModelDropdown,
  ModelDropdownAuto,
  RangeFilters,
} from '../components/ModelFilters';
import {CollegeSortable} from '../api/college';
import SearchBar from '../components/SearchBar';

const CollegeScreen = () => {
  const emptyIter = Array.from(Array(4).keys());

  const [colleges, setColleges] = useState<College[]>([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  const paramID = parseInt(useParams<{id: string}>().id || '-1');
  const [id, setId] = useState(paramID);
  const [isOpen, setIsOpen] = useState(id !== -1);

  const [queryFailed, setQueryFailed] = useState(false);

  const [filters, setFilters] = useState({
    search: '',
    sortValue: 'Sort By',
    state: 'Select State',
    city: '',
    minAdmission: 0,
    maxAdmission: 100,
    minSize: 0,
    maxSize: 65000,
    minInStateTuition: 0,
    maxInStateTuition: 100000,
    minOutStateTuition: 0,
    maxOutStateTuition: 100000,
    minSAT: 400,
    maxSAT: 1600,
  });

  const [isReset, setIsReset] = useState(true);

  const rangeFilters = [
    {
      displayName: 'Admission Rate',
      filterName: 'Admission',
      rangeMin: 0,
      rangeMax: 100,
      minVal: filters.minAdmission,
      maxVal: filters.maxAdmission,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minAdmission');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxAdmission');
      },
      symbol: '%',
    },
    {
      displayName: 'Student Size',
      filterName: 'Size',
      rangeMin: 0,
      rangeMax: 65000,
      minVal: filters.minSize,
      maxVal: filters.maxSize,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minSize');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxSize');
      },
    },
    {
      displayName: 'In-State Tuition',
      filterName: 'InStateTuition',
      rangeMin: 0,
      rangeMax: 100000,
      minVal: filters.minInStateTuition,
      maxVal: filters.maxInStateTuition,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minInStateTuition');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxInStateTuition');
      },
      symbol: '$',
    },
    {
      displayName: 'Out-of-State Tuition',
      filterName: 'OutStateTuition',
      rangeMin: 0,
      rangeMax: 100000,
      minVal: filters.minOutStateTuition,
      maxVal: filters.maxOutStateTuition,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minOutStateTuition');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxOutStateTuition');
      },
      symbol: '$',
    },
    {
      displayName: 'SAT Score',
      filterName: 'SAT',
      rangeMin: 400,
      rangeMax: 1600,
      minVal: filters.minSAT,
      maxVal: filters.maxSAT,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minSAT');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxSAT');
      },
      steps: 10,
    },
  ];

  const applySearchFilters = () => {
    setColleges([]);
    getColleges();
  };

  useEffect(() => {
    getColleges();
  }, []);

  useEffect(() => {
    applySearchFilters();
  }, [
    page,
    filters.search,
    filters.sortValue,
    filters.city,
    filters.state,
    isReset,
  ]);

  const handleFilterChange = (e: ChangeEvent | string, filter: string) => {
    let newValue =
      typeof e === 'string'
        ? e
        : e.target
        ? (e.target as HTMLInputElement).value
        : '';
    const newFilters =
      filter === 'state'
        ? {...filters, state: newValue, city: ''}
        : {...filters, [filter]: newValue};
    setFilters(newFilters);
  };
  const handleSliderChange = (e: Event, newValue: number[], filter: string) => {
    const newFilters = {
      ...filters,
      ['min' + filter]: newValue[0],
      ['max' + filter]: newValue[1],
    };
    setFilters(newFilters);
  };

  const reset = () => {
    setFilters({
      search: '',
      state: 'Select State',
      city: '',
      minAdmission: 0,
      maxAdmission: 100,
      minSize: 0,
      maxSize: 65000,
      minInStateTuition: 0,
      maxInStateTuition: 100000,
      minOutStateTuition: 0,
      maxOutStateTuition: 100000,
      minSAT: 400,
      maxSAT: 1600,
      sortValue: 'Sort By',
    });

    setIsReset(!isReset);
  };

  function closeModal() {
    setIsOpen(false);
  }

  function openModal(collegeId: number) {
    setId(collegeId);
    setIsOpen(true);
  }

  const getColleges = async () => {
    const sort =
      filters.sortValue === 'Sort By'
        ? ''
        : filters.sortValue
            .toLowerCase()
            .replaceAll('-', '_')
            .replaceAll(' ', '_');
    await CollegeAPI.getColleges({
      page: page,
      search: filters.search,
      sort: sort as CollegeSortable,
      exactFilters: {
        state: filters.state === 'Select State' ? '' : filters.state,
        city: filters.city,
      },
      rangeFilters: {
        min: {
          admission_rate: filters.minAdmission / 100,
          size: filters.minSize,
          in_state_tuition: filters.minInStateTuition,
          out_of_state_tuition: filters.minOutStateTuition,
          sat_score: filters.minSAT,
        },
        max: {
          admission_rate: filters.maxAdmission / 100,
          size: filters.maxSize,
          in_state_tuition: filters.maxInStateTuition,
          out_of_state_tuition: filters.maxOutStateTuition,
          sat_score: filters.maxSAT,
        },
      },
    }).then(res => {
      const fetchedColleges = res.results;
      const numPages = res.metadata.total_pages;
      setColleges(fetchedColleges);
      setTotalPages(numPages);
      setTotalItems(res.metadata.total_items);
      setQueryFailed(fetchedColleges.length === 0);
    });
  };

  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Colleges
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Whether you're a student looking for the right college to attend, or
            simply interested in exploring the different campuses in the area,
            our page offers detailed information about each university.
          </p>
          <div className="flex flex-wrap w-full">
            <SearchBar
              value={filters.search}
              onChange={(e: ChangeEvent) => handleFilterChange(e, 'search')}
              placeholder={'Search Colleges'}
            />
            <ModelDropdown
              chosenVal={filters.sortValue}
              setChosenVal={(e: ChangeEvent) =>
                handleFilterChange(e, 'sortValue')
              }
              placeholder="Sort By"
              defVal="SAT Score"
              vals={CollegeSort}
            />
            <ModelDropdown
              chosenVal={filters.state}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'state')}
              placeholder="Select State"
              defVal="Select State"
              vals={StatesAbbr}
            />
            <ModelDropdownAuto
              chosenVal={filters.city}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'city')}
              placeholder="Select City"
              defVal="Select City"
              vals={Cities(filters.state)}
            />
            <RangeFilters
              filters={rangeFilters}
              handleSliderChange={handleSliderChange}
              applyFilters={applySearchFilters}
            />
            <button className="btn" onClick={reset}>
              {' '}
              Reset Search and Filters
            </button>
          </div>
        </div>
        <div className="md:flex flex-wrap -m-4">
          {colleges.length > 0 ? (
            colleges.map((college, index) => (
              <div
                key={index}
                onClick={() => openModal(college.id)}
                className="xl:w-1/4 md:w-1/2 p-4 block cursor-pointer"
              >
                <CollegeCard college={college} search={filters.search} />
              </div>
            ))
          ) : !queryFailed ? (
            emptyIter.map((_, index) => (
              <div key={index} className="xl:w-1/4 md:w-1/2 p-4 block">
                <CollegeCardSkel />
              </div>
            ))
          ) : (
            <div className="w-full text-center">No results found</div>
          )}
        </div>
        {colleges.length > 0 && (
          <div className="pt-14">
            <PaginationBar
              page={page}
              setPage={setPage}
              totalPages={totalPages}
              totalItems={totalItems}
            />
          </div>
        )}
      </div>
      <InstanceModal
        instance={<CollegeInstance id={id} />}
        isOpen={isOpen}
        closeModal={closeModal}
      />
    </section>
  );
};

export default CollegeScreen;
