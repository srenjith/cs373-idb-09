import axios from 'axios';
import {useEffect, useState} from 'react';
import {TeamMember, apisInfo, teamInfo, toolsInfo} from '../static/AboutInfo';
import {MemberCard} from '../components/memberCard';
import {ToolCard} from '../components/toolCard';
import breakfast from '../assets/breakfast.svg';
import pplInLine from '../assets/pplInLine.svg';
import streetFood from '../assets/streetFood.svg';

type RepoStats = {
  totalCommits: number;
  totalIssues: number;
  totalTests: number;
};

const client = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
  headers: {Authorization: 'Bearer glpat-at_Tn_1vnki1G4zs-Ncv'},
});

const fetchGitlabStats = async () => {
  let totalCommits = 0;
  let totalIssues = 0;
  let totalTests = 39;

  const {data: contributors} = await client.get(
    '/projects/43387727/repository/contributors'
  );
  const {data: ourIssues} = await client.get('/projects/43387727/issues');
  const {data: developerIssues} = await client.get('/projects/43345538/issues');

  const allIssues = [...ourIssues, ...developerIssues];

  contributors.forEach((contributor: any) => {
    const teamMember = teamInfo.find(
      member => member.email === contributor.email
    );
    if (teamMember) {
      teamMember.commits += contributor.commits;
      totalCommits += contributor.commits;
    }
  });

  allIssues.forEach((issue: any) => {
    const author = issue.author;
    const teamMember = teamInfo.find(
      member => member.username === author.username
    );
    if (teamMember) {
      teamMember.issues += 1;
      totalIssues += 1;
    }
  });
  return {
    teamInfo,
    repoStats: {totalCommits, totalIssues, totalTests},
  };
};

const RepoDataCard = ({
  title,
  value,
}: {
  title: string;
  value: number | undefined;
}) => {
  return (
    <div className="w-1/4 ">
      <div className="  text-center bg-accent-content rounded-lg p-4">
        <div className="stat-title">{title}</div>
        <div className="stat-value">{value}</div>
      </div>
    </div>
  );
};

const About = () => {
  const [team, setTeam] = useState<TeamMember[]>(teamInfo);
  const [repoStats, setRepoStats] = useState<RepoStats>();

  useEffect(() => {
    fetchGitlabStats().then(({teamInfo, repoStats}) => {
      setTeam(teamInfo);
      setRepoStats(repoStats);
    });
  }, []);

  return (
    <div className="animate-fade-in-up">
      <div className="hero min-h-[50vh] bg-base-200">
        <div className="hero-content flex-col lg:flex-row">
          <div className="md:w-4/5 rounded-t-lg md:rounded-lg">
            <img src={pplInLine} alt="people in line" />
          </div>
          <div>
            <h1 className="text-3xl md:text-4xl font-semibold">
              Explore Universities, Local Events, and Hidden Food Gems!
            </h1>
            <p className="py-4">
              Welcome to Campus Eats & Treats, your ultimate guide to
              discovering the best universities, events, and local food spots
              for your next weekend adventure!
              <br />
              <br />
              Whether you're looking to explore the vibrant campus life of a new
              university, attend a local festival or concert, or try out some
              delicious local cuisine, Campus Eats & Treats has got you covered.
            </p>
          </div>
        </div>
      </div>
      <div className="container px-5 py-12 mx-auto">
        <div className="flex flex-wrap w-full mb-10">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl title-font mb-2 font-semibold">
              Meet the project team!
            </h1>
            <div className="h-1 w-20 bg-accent-content rounded"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed">
            Get to know us and discover the people behind Campus Eats & Treats
            who are committed to bringing you the best content and
            recommendations for your next weekend plans.
          </p>
        </div>
        <div className="flex flex-wrap -m-4">
          {team.map((member, i) => (
            <MemberCard key={i} member={member} />
          ))}
        </div>
      </div>
      <div className="container px-5 pb-24 pt-8 mx-auto">
        <div className="flex flex-wrap w-full mb-10">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl title-font mb-2 font-semibold">
              Behind the scenes
            </h1>
            <div className="h-1 w-20 bg-accent-content rounded mb-4"></div>
            <div className="flex">
              <a
                href="https://gitlab.com/srenjith/cs373-idb-09"
                target="_blank"
                className="block bg-base-300 focus:ring-4 font-semibold rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 hover:scale-105 transition-all"
              >
                REPOSITORY
              </a>
              <a
                href="https://documenter.getpostman.com/view/18944196/2s93CExca5"
                target="_blank"
                className="block bg-base-300 focus:ring-4 font-semibold rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 hover:scale-105 transition-all"
              >
                POSTMAN DOCUMENTATION
              </a>
            </div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed">
            These statistics and information showcase the work put into
            developing the project, and highlight the technologies and resources
            utilized to create an effective and efficient solution.
          </p>
        </div>
        <div className="flex flex-wrap justify-between mb-8">
          <RepoDataCard title="Total Commits" value={repoStats?.totalCommits} />
          <RepoDataCard title="Total Issues" value={repoStats?.totalIssues} />
          <RepoDataCard title="Total Tests" value={repoStats?.totalTests} />
        </div>
        <div className="flex flex-wrap mb-4">
          {toolsInfo.map((tool, i) => (
            <ToolCard key={i} tool={tool} type={'TOOL'} />
          ))}
          {apisInfo.map((tool, i) => (
            <ToolCard key={i} tool={tool} type={'API'} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default About;
