import CollegeTuitionVisualization from '../visualizations/CollegeTuition';
import DateMinPriceVisualization from '../visualizations/EventMinPriceVisualization';
import EventsStatesVisualization from '../visualizations/EventsStates';
import RestaurantPriceToRatingVisualization from '../visualizations/RestaurantPriceToRating';

const OurVisualizationScreen = () => {
  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Our Visualizations
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded mb-2"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Here's some data visualizations created by the data you just
            explored.
          </p>
        </div>
        <div className="flex flex-wrap gap-4 justify-around">
          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              College Tuition Distribution
            </h1>
            <CollegeTuitionVisualization />
          </div>

          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              Restaurant Price VS Rating
            </h1>
            <RestaurantPriceToRatingVisualization />
          </div>
          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              Event Min Price vs Month
            </h1>
            <DateMinPriceVisualization />
          </div>
          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              Events per State
            </h1>
            <EventsStatesVisualization />
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurVisualizationScreen;
