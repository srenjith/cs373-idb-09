import AdmissionStudentBodyVisualization from '../visualizations/ProviderAdmissionStudentBodyVisualization';
import ProviderSATAdmissionVisualization from '../visualizations/ProviderSATAdmissionVisualization';
import ProviderSATStudentBodyVisualization from '../visualizations/ProviderSATStudentBodyVisualization';

const ProviderVisualizationScreen = () => {
  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Provider Visualizations
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded mb-2"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Here's some data visualizations created using our provider's APIs.
          </p>
        </div>
        <div className="flex flex-wrap gap-4 justify-around">
          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              SAT VS Admissions
            </h1>
            <ProviderSATAdmissionVisualization />
          </div>

          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              SAT VS Student Body
            </h1>
            <ProviderSATStudentBodyVisualization />
          </div>
          <div className=" bg-white p-4 rounded-lg pr-8">
            <h1 className="md:text-xl opacity-50 text-center text-lg font-semibold title-font mb-2 ">
              Admissions VS Student Body
            </h1>
            <AdmissionStudentBodyVisualization />
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProviderVisualizationScreen;
