import {ChangeEvent, useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import CollegeAPI from '../api/college';
import RestaurantAPI from '../api/restaurant';
import EventAPI from '../api/event';
import {CollegeCard, CollegeCardSkel} from '../components/CollegeCard';
import {EventCard, EventCardSkel} from '../components/EventCard';
import {RestaurantCard, RestaurantCardSkel} from '../components/RestaurantCard';
import PaginationBar from '../components/paginationBar';
import SearchBar from '../components/SearchBar';

type ModelData = {
  modelName: string;
  modelInstances: any[];
  page: number;
  totalPages: number;
  totalItems: number;
  queryFailed: boolean;
  pageChanging: boolean;
};

const AllSearchResults = () => {
  const query = useParams<{query: string}>().query;
  const [searchTerm, setSearchTerm] = useState(query);

  const emptyIter = Array.from(Array(4).keys());

  const [collegeData, setCollegeData] = useState<ModelData>({
    modelName: 'Colleges',
    modelInstances: [],
    page: 1,
    totalPages: 1,
    totalItems: 1,
    queryFailed: false,
    pageChanging: false,
  });
  const [restaurantData, setRestaurantData] = useState<ModelData>({
    modelName: 'Restaurants',
    modelInstances: [],
    page: 1,
    totalPages: 1,
    totalItems: 1,
    queryFailed: false,
    pageChanging: false,
  });
  const [eventData, setEventData] = useState<ModelData>({
    modelName: 'Events',
    modelInstances: [],
    page: 1,
    totalPages: 1,
    totalItems: 1,
    queryFailed: false,
    pageChanging: false,
  });

  const models = [collegeData, restaurantData, eventData];

  const getCollegeData = async () => {
    await CollegeAPI.getColleges({
      page: models[0].page,
      search: searchTerm,
      perPage: 4,
    }).then(res => {
      setCollegeData({
        ...collegeData,
        modelInstances: res.results,
        page: res.metadata.page,
        totalPages: res.metadata.total_pages,
        totalItems: res.metadata.total_items,
        queryFailed: res.results.length === 0,
        pageChanging: false,
      });
    });
  };

  const getRestaurantData = async () => {
    await RestaurantAPI.getRestaurants({
      page: models[1].page,
      search: searchTerm,
      perPage: 4,
    }).then(res => {
      setRestaurantData({
        ...restaurantData,
        modelInstances: res.results,
        page: res.metadata.page,
        totalPages: res.metadata.total_pages,
        totalItems: res.metadata.total_items,
        queryFailed: res.results.length === 0,
        pageChanging: false,
      });
    });
  };

  const getEventData = async () => {
    await EventAPI.getEvents({
      page: models[2].page,
      search: searchTerm,
      perPage: 4,
    }).then(res => {
      setEventData({
        ...eventData,
        modelInstances: res.results,
        page: res.metadata.page,
        totalPages: res.metadata.total_pages,
        totalItems: res.metadata.total_items,
        queryFailed: res.results.length === 0,
        pageChanging: false,
      });
    });
  };

  const handlePageChange = (page: number, idx: number) => {
    switch (idx) {
      case 0:
        setCollegeData({...collegeData, page: page});
        break;
      case 1:
        setRestaurantData({...restaurantData, page: page});
        break;
      case 2:
        setEventData({...eventData, page: page});
        break;
    }
  };

  const getAllData = () => {
    getCollegeData();
    getRestaurantData();
    getEventData();
  };

  useEffect(() => {
    getAllData();
  }, []);

  useEffect(() => {
    setCollegeData({
      modelName: 'Colleges',
      modelInstances: [],
      page: 1,
      totalPages: 1,
      totalItems: 1,
      queryFailed: false,
      pageChanging: false,
    });
    setRestaurantData({
      modelName: 'Restaurants',
      modelInstances: [],
      page: 1,
      totalPages: 1,
      totalItems: 1,
      queryFailed: false,
      pageChanging: false,
    });
    setEventData({
      modelName: 'Events',
      modelInstances: [],
      page: 1,
      totalPages: 1,
      totalItems: 1,
      queryFailed: false,
      pageChanging: false,
    });
    getAllData();
  }, [searchTerm]);

  useEffect(() => {
    setCollegeData({...collegeData, modelInstances: [], pageChanging: true});
    getCollegeData();
  }, [collegeData.page]);
  useEffect(() => {
    setRestaurantData({
      ...restaurantData,
      modelInstances: [],
      pageChanging: true,
    });
    getRestaurantData();
  }, [restaurantData.page]);
  useEffect(() => {
    setEventData({...eventData, modelInstances: [], pageChanging: true});
    getEventData();
  }, [eventData.page]);

  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-6">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2 ">
              Search Results
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded mb-2"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            Browse through colleges, restaurants, and events all at once. With
            just a few clicks, you can find the perfect college, restaurant, and
            event that fits your preferences and schedule.
          </p>
          <SearchBar
            value={searchTerm}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setSearchTerm(e.target.value)
            }
            placeholder="Search"
          />
        </div>
        {models.map((model, index) => (
          <div key={index} className="mb-10">
            <h2 className="text-2xl font-semibold opacity-70 mb-6">
              {model.modelName}
            </h2>
            <div className="md:flex overflow-scroll -m-4">
              {model.modelInstances.length > 0 ? (
                model.modelInstances.map((modelInstance, index) => (
                  <a
                    key={index}
                    href={`/${model.modelName.toLowerCase()}/${
                      modelInstance.id
                    }`}
                    className="xl:w-1/4 flex-wrap xl:min-w-[30ch] md:w-1/2 p-4 block cursor-pointer "
                  >
                    {model.modelName === 'Colleges' ? (
                      <CollegeCard
                        college={modelInstance}
                        search={searchTerm}
                      />
                    ) : model.modelName === 'Restaurants' ? (
                      <RestaurantCard
                        restaurant={modelInstance}
                        search={searchTerm}
                      />
                    ) : model.modelName === 'Events' ? (
                      <EventCard event={modelInstance} search={searchTerm} />
                    ) : null}
                  </a>
                ))
              ) : !model.queryFailed ? (
                emptyIter.map((_, index) => (
                  <div key={index} className="xl:w-1/4 md:w-1/2 p-4 block">
                    {model.modelName === 'Colleges' ? (
                      <CollegeCardSkel />
                    ) : model.modelName === 'Restaurants' ? (
                      <RestaurantCardSkel />
                    ) : model.modelName === 'Events' ? (
                      <EventCardSkel />
                    ) : null}
                  </div>
                ))
              ) : (
                <div className="w-full text-center">No results found</div>
              )}
            </div>
            {(model.modelInstances.length > 0 || model.pageChanging) && (
              <div className="pt-10">
                <PaginationBar
                  page={model.page}
                  setPage={page => handlePageChange(page, index)}
                  totalPages={model.totalPages}
                  totalItems={model.totalItems}
                />
              </div>
            )}
          </div>
        ))}
      </div>
    </section>
  );
};

export default AllSearchResults;
