import {ChangeEvent, useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {RestaurantCard, RestaurantCardSkel} from '../components/RestaurantCard';
import PaginationBar from '../components/paginationBar';
import InstanceModal from '../components/InstanceModal';
import RestaurantInstance from './RestaurantInstanceScreen';
import {Restaurant, RestaurantAPI} from '../api/API';
import {
  ModelDropdown,
  ModelDropdownAuto,
  RangeFilterProps,
  RangeFilters,
} from '../components/ModelFilters';
import {RestaurantSortable} from '../api/restaurant';
import {
  Cities,
  RestaurantSort,
  StatesAbbr,
} from '../static/SearchSortFilterData';
import SearchBar from '../components/SearchBar';

const RestaurantScreen = () => {
  const emptyIter = Array.from(Array(4).keys());
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  const paramID = parseInt(useParams<{id: string}>().id || '-1');
  const [id, setId] = useState(paramID);
  const [isOpen, setIsOpen] = useState(id !== -1);

  const [queryFailed, setQueryFailed] = useState(false);

  const [filters, setFilters] = useState({
    search: '',
    sortValue: 'Sort By',
    state: 'Select State',
    city: '',
    category: '',
    minRating: 0,
    maxRating: 5,
    minPrice: 1,
    maxPrice: 4,
  });

  const [isReset, setIsReset] = useState(false);

  const categories: string[] =
    filters.category === '' ? ['Select Cuisine'] : [];

  const rangeFilters: RangeFilterProps[] = [
    {
      displayName: 'Rating',
      filterName: 'Rating',
      rangeMin: 0,
      rangeMax: 5,
      minVal: filters.minRating,
      maxVal: filters.maxRating,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minRating');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxRating');
      },
      steps: 0.5,
    },
    {
      displayName: 'Price',
      filterName: 'Price',
      rangeMin: 1,
      rangeMax: 4,
      minVal: filters.minPrice,
      maxVal: filters.maxPrice,
      setMinVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'minPrice');
      },
      setMaxVal: (e: ChangeEvent) => {
        handleFilterChange(e, 'maxPrice');
      },
      pictorial: true,
      symbol: '$',
    },
  ];

  const applySearchFilters = () => {
    setRestaurants([]);
    getRestaurants();
  };

  useEffect(() => {
    getRestaurants();
  }, []);

  useEffect(() => {
    applySearchFilters();
  }, [page, filters.search, filters.sortValue, filters.state, filters.city]);

  const handleFilterChange = (e: ChangeEvent | string, filter: string) => {
    let newValue =
      typeof e === 'string'
        ? e
        : e.target
        ? (e.target as HTMLInputElement).value
        : '';
    const newFilters =
      filter === 'state'
        ? {...filters, state: newValue, city: ''}
        : {...filters, [filter]: newValue};
    setFilters(newFilters);
  };

  const handleSliderChange = (e: Event, newValue: number[], filter: string) => {
    const newFilters = {
      ...filters,
      ['min' + filter]: newValue[0],
      ['max' + filter]: newValue[1],
    };
    setFilters(newFilters);
  };

  const reset = () => {
    setFilters({
      search: '',
      sortValue: 'Sort By',
      state: 'Select State',
      city: '',
      category: '',
      minRating: 0,
      maxRating: 5,
      minPrice: 1,
      maxPrice: 3,
    });

    setIsReset(!isReset);
  };

  function closeModal() {
    setIsOpen(false);
  }

  function openModal(collegeId: number) {
    setId(collegeId);
    setIsOpen(true);
  }

  const getRestaurants = async () => {
    const sort =
      filters.sortValue === 'Sort By'
        ? ''
        : filters.sortValue
            .toLowerCase()
            .replaceAll('-', '_')
            .replaceAll(' ', '_');
    await RestaurantAPI.getRestaurants({
      page: page,
      search: filters.search,
      sort: sort as RestaurantSortable,
      exactFilters: {
        state: filters.state === 'Select State' ? '' : filters.state,
        city: filters.city,
      },
      rangeFilters: {
        min: {
          rating: filters.minRating,
          price: filters.minPrice,
        },
        max: {
          rating: filters.maxRating,
          price: filters.maxPrice,
        },
      },
    }).then(res => {
      const fetchedRestaurants = res.results;
      const numPages = res.metadata.total_pages;
      setRestaurants(fetchedRestaurants);
      setTotalPages(numPages);
      setTotalItems(res.metadata.total_items);
      setQueryFailed(fetchedRestaurants.length === 0);
    });
  };

  return (
    <section className="min-h-[90vh] animate-fade-in-up">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-20">
          <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
            <h1 className="sm:text-3xl text-2xl font-semibold title-font mb-2">
              Restaurants
            </h1>
            <div className="h-1 w-20 bg-indigo-500 rounded"></div>
          </div>
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500 mb-4">
            We know that finding great food can be a challenge, especially when
            you're in a new city or town. So explore our Restaurants page and
            discover some of the best food spots around. We guarantee that your
            taste buds will thank you!
          </p>
          <div className="flex flex-wrap w-full">
            <SearchBar
              value={filters.search}
              onChange={(e: ChangeEvent) => handleFilterChange(e, 'search')}
              placeholder={'Search Restaurants'}
            />
            <ModelDropdown
              chosenVal={filters.sortValue}
              setChosenVal={(e: ChangeEvent) =>
                handleFilterChange(e, 'sortValue')
              }
              placeholder="Sort By"
              defVal="Sort By"
              vals={RestaurantSort}
            />
            <ModelDropdown
              chosenVal={filters.state}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'state')}
              placeholder="Select State"
              defVal="Select State"
              vals={StatesAbbr}
            />
            <ModelDropdownAuto
              chosenVal={filters.city}
              setChosenVal={(e: ChangeEvent) => handleFilterChange(e, 'city')}
              placeholder="Select City"
              defVal="Select City"
              vals={Cities(filters.state)}
            />
            <RangeFilters
              filters={rangeFilters}
              handleSliderChange={handleSliderChange}
              applyFilters={applySearchFilters}
            />
            <button className="btn" onClick={reset}>
              {' '}
              Reset Search and Filters
            </button>
          </div>
        </div>
        <div className="md:flex flex-wrap -m-4">
          {restaurants.length > 0 ? (
            restaurants.map((restaurant, index) => (
              <div
                key={index}
                onClick={() => openModal(restaurant.id)}
                className="xl:w-1/4 md:w-1/2 p-4 block cursor-pointer"
              >
                <RestaurantCard
                  restaurant={restaurant}
                  search={filters.search}
                />
              </div>
            ))
          ) : !queryFailed ? (
            emptyIter.map((_, index) => (
              <div key={index} className="xl:w-1/4 md:w-1/2 p-4 block">
                <RestaurantCardSkel />
              </div>
            ))
          ) : (
            <div className="w-full text-center">No results found</div>
          )}
        </div>
        {restaurants.length > 0 && (
          <div className="pt-14">
            <PaginationBar
              page={page}
              setPage={setPage}
              totalPages={totalPages}
              totalItems={totalItems}
            />
          </div>
        )}
        <InstanceModal
          instance={<RestaurantInstance id={id} />}
          isOpen={isOpen}
          closeModal={closeModal}
        />
      </div>
    </section>
  );
};

export default RestaurantScreen;
