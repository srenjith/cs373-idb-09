import {Route, Routes} from 'react-router-dom';

import Home from './Home';
import About from './About';
import CollegeScreen from './CollegeScreen';
import RestaurantScreen from './RestaurantScreen';
import EventScreen from './EventScreen';
import AllSearch from './AllSearch';
import AllSearchResults from './AllSearchResults';
import OurVisualizationScreen from './OurVisualizations';
import ProviderVisualizationScreen from './ProviderVisualizations';

const RouterSwitch = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/colleges" element={<CollegeScreen />} />
      <Route path="/colleges/:id" element={<CollegeScreen />} />
      <Route path="/restaurants" element={<RestaurantScreen />} />
      <Route path="/restaurants/:id" element={<RestaurantScreen />} />
      <Route path="/events" element={<EventScreen />} />
      <Route path="/events/:id" element={<EventScreen />} />
      <Route path="/search" element={<AllSearch />} />
      <Route path="/search/:query" element={<AllSearchResults />} />
      <Route path="/our-visualizations" element={<OurVisualizationScreen />} />
      <Route
        path="/provider-visualizations"
        element={<ProviderVisualizationScreen />}
      />
    </Routes>
  );
};

export default RouterSwitch;
