import NavBar from './components/navBar';
import RouterSwitch from './views/RouterSwitch';
import Footer from './components/Footer';
import {BrowserRouter as Router} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="bg-base-200 min-h-screen scroll-smooth">
        <NavBar />
        <RouterSwitch />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
