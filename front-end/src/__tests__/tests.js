import '@testing-library/jest-dom';
import {render, screen} from '@testing-library/react';

import Home from '../App';
import About from '../views/About';
import CollegeScreen from '../views/CollegeScreen';
import RestaurantScreen from '../views/RestaurantScreen';
import EventScreen from '../views/EventScreen';
import CollegeInstanceScreen from '../views/CollegeInstanceScreen';
import RestaurantInstanceScreen from '../views/RestaurantInstanceScreen';
import EventInstanceScreen from '../views/EventInstanceScreen';
import {BrowserRouter} from 'react-router-dom';

describe('Basic rendering and display tests', () => {
  // Test 1
  it('Navigation bar renders all elements', () => {
    render(<Home />);
    expect(screen.getByText('Campus Eats & Treats')).toBeInTheDocument();
    expect(screen.getAllByText('Home')[0]).toBeInTheDocument();
    expect(screen.getAllByText('About')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Explore')[0]).toBeInTheDocument();
  });

  // Test 2
  it('Home page displays text correctly', () => {
    render(<Home />);
    expect(screen.getByText('Find something fun to do!')).toBeInTheDocument();
    expect(screen.getByText('Explore your area')).toBeInTheDocument();
  });

  // Test 3
  it('About page displays text correctly', () => {
    render(<About />);
    expect(screen.getByText('Meet the project team!')).toBeInTheDocument();
    expect(screen.getByText('Behind the scenes')).toBeInTheDocument();
  });

  // Test 4
  it('College screen displays text correctly', () => {
    render(<CollegeScreen />);
    expect(screen.getByText('Colleges')).toBeInTheDocument();
    expect(
      screen.getByText(
        "Whether you're a student looking for the right college to attend, or simply interested in exploring the different campuses in the area, our page offers detailed information about each university."
      )
    ).toBeInTheDocument();
  });

  // Test 5
  it('Restaurant screen displays text correctly', () => {
    render(<RestaurantScreen />);
    expect(screen.getByText('Restaurants')).toBeInTheDocument();
    expect(
      screen.getByText(
        "We know that finding great food can be a challenge, especially when you're in a new city or town. So explore our Restaurants page and discover some of the best food spots around. We guarantee that your taste buds will thank you!"
      )
    ).toBeInTheDocument();
  });

  // Test 6
  it('Event screen displays text correctly', () => {
    render(<EventScreen />);
    expect(screen.getByText('Events')).toBeInTheDocument();
    expect(
      screen.getByText(
        "Browse our Events page and discover the best ways to spend your free time in the area. From music and art to food and culture, there's always something new and exciting to experience. Don't miss out on the fun!"
      )
    ).toBeInTheDocument();
  });
});

describe('Snapshot tests', () => {
  // Test 7
  it('Home', () => {
    const tree = (
      <BrowserRouter>
        renderer.create(
        <Home />
        ).toJSON()
      </BrowserRouter>
    );
    expect(tree).toMatchSnapshot();
  });

  // Test 8
  it('About', () => {
    const tree = (
      <BrowserRouter>
        renderer.create(
        <About />
        ).toJSON()
      </BrowserRouter>
    );
    expect(tree).toMatchSnapshot();
  });

  // Test 9
  it('College Instance', () => {
    const tree = (
      <BrowserRouter>
        renderer.create(
        <CollegeInstanceScreen />
        ).toJSON()
      </BrowserRouter>
    );
    expect(tree).toMatchSnapshot();
  });

  // Test 10
  it('Restaurant Instance', () => {
    const tree = (
      <BrowserRouter>
        renderer.create(
        <RestaurantInstanceScreen />
        ).toJSON()
      </BrowserRouter>
    );
    expect(tree).toMatchSnapshot();
  });

  // Test 11
  it('Event Instance', () => {
    const tree = (
      <BrowserRouter>
        renderer.create(
        <EventInstanceScreen />
        ).toJSON()
      </BrowserRouter>
    );
    expect(tree).toMatchSnapshot();
  });
});

describe('Searching, sorting, and filtering tests', () => {
  // Test 12
  it('Searching colleges', () => {
    render(<CollegeScreen />);
    expect(screen.getByPlaceholderText('Search Colleges')).toBeInTheDocument();
  });

  // Test 13
  it('Sorting colleges', () => {
    render(<CollegeScreen />);
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  });

  // Test 14
  it('Filtering colleges', () => {
    render(<CollegeScreen />);
    expect(screen.getByText('Select State')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Select City')).toBeInTheDocument();
    expect(screen.getByText('More Filters')).toBeInTheDocument();
  });

  // Test 15
  it('Searching restaurants', () => {
    render(<RestaurantScreen />);
    expect(
      screen.getByPlaceholderText('Search Restaurants')
    ).toBeInTheDocument();
  });

  // Test 16
  it('Sorting restaurants', () => {
    render(<RestaurantScreen />);
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  });

  // Test 17
  it('Filtering restaurants', () => {
    render(<RestaurantScreen />);
    expect(screen.getByText('Select State')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Select City')).toBeInTheDocument();
    expect(screen.getByText('More Filters')).toBeInTheDocument();
  });

  // Test 18
  it('Searching events', () => {
    render(<EventScreen />);
    expect(screen.getByPlaceholderText('Search Events')).toBeInTheDocument();
  });

  // Test 19
  it('Sorting events', () => {
    render(<EventScreen />);
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  });

  // Test 20
  it('Filtering events', () => {
    render(<EventScreen />);
    expect(screen.getByText('Select State')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Select City')).toBeInTheDocument();
    expect(
      screen.getByPlaceholderText('YYYY-MM-DD to YYYY-MM-DD')
    ).toBeInTheDocument();
    expect(screen.getByText('More Filters')).toBeInTheDocument();
  });
});
