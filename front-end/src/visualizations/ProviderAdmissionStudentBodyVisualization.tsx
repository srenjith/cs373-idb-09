import {
  CartesianGrid,
  Label,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import {useEffect, useState} from 'react';
import {
  ProviderCollege,
  getCollegesSortedByStudentBodySize,
} from '../api/ProviderAPI';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-bold whitespace-nowrap">{item.name}</p>
        <p className="text-gray-900 whitespace-nowrap">Student Body Size: {item.y}</p>
        <p className="text-gray-900 whitespace-nowrap">
          Admission Rate: {Math.round(item.x * 100) / 100}%
        </p>
      </div>
    );
  }

  return null;
};

const AdmissionStudentBodyVisualization = () => {
  const [collegeData, setCollegeData] = useState<ProviderCollege[]>([]);

  useEffect(() => {
    getCollegesSortedByStudentBodySize().then(data => {
      setCollegeData(data);
    });
  }, []);

  return (
    <ScatterChart
      width={350}
      height={350}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      }}
    >
      <CartesianGrid />
      <XAxis
        type="number"
        dataKey="x"
        name="Admission Rate"
        unit="%"
        domain={[0, 100]}
        reversed
      >
        <Label value="Admission Rate" offset={-10} position="insideBottom" />
      </XAxis>
      <YAxis
        type="number"
        dataKey="y"
        name="Score"
        unit=""
        domain={[20000, 80000]}
      >
        <Label
          value="Student Body Size"
          position="insideLeft"
          angle={-90}
          offset={-10}
        />
      </YAxis>
      <Tooltip content={<CustomTooltip />} cursor={{strokeDasharray: '3 3'}} />
      <Scatter
        name="A school"
        data={collegesToDataPoints(collegeData)}
        fill="#6367F1"
      />
    </ScatterChart>
  );
};

const collegesToDataPoints = (colleges: ProviderCollege[]) => {
  return colleges.map(college => {
    return {
      x: college.admission_rate * 100,
      y: college.studentBodySize,
      name: college.name,
    };
  });
};

export default AdmissionStudentBodyVisualization;
