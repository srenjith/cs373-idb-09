import {
  CartesianGrid,
  Label,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import {useEffect, useState} from 'react';
import {ProviderCollege} from '../api/ProviderAPI';

import {getCollegesSortedBySAT} from '../api/ProviderAPI';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-bold whitespace-nowrap">{item.name}</p>
        <p className="text-gray-900 whitespace-nowrap">SAT Score: {item.y}</p>
        <p className="text-gray-900 whitespace-nowrap">
          Admission Rate: {Math.round(item.x * 100) / 100}%
        </p>
      </div>
    );
  }

  return null;
};

const SATAdmissionVisualization = () => {
  const [collegeData, setCollegeData] = useState<ProviderCollege[]>([]);

  useEffect(() => {
    getCollegesSortedBySAT().then(data => {
      setCollegeData(data);
    });
  }, []);

  return (
    <ScatterChart
      width={350}
      height={350}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      }}
    >
      <CartesianGrid />
      <XAxis
        type="number"
        dataKey="x"
        name="Admission Rate"
        unit="%"
        domain={[0, 100]}
        reversed
      >
        <Label value="Admission Rate" offset={-10} position="insideBottom" />
      </XAxis>
      <YAxis
        type="number"
        dataKey="y"
        name="Score"
        unit=""
        domain={[1400, 1600]}
      >
        <Label value="SAT Score" position="insideLeft" angle={-90} />
      </YAxis>
      <Tooltip content={<CustomTooltip />} cursor={{strokeDasharray: '3 3'}} />
      <Scatter
        name="A school"
        data={collegesToDataPoints(collegeData)}
        fill="#6367F1"
      />
    </ScatterChart>
  );
};

const collegesToDataPoints = (colleges: ProviderCollege[]) => {
  return colleges.map(college => {
    return {
      x: college.admission_rate * 100,
      y: college.sat_avg,
      name: college.name,
    };
  });
};

export default SATAdmissionVisualization;
