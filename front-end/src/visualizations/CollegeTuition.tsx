import {useEffect, useState} from 'react';
import {Pie, PieChart, Tooltip} from 'recharts';
import CollegeAPI, {College} from '../api/college';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-semibold whitespace-nowrap text-center">
          {item.name}
        </p>
        <p className="text-gray-900 whitespace-nowrap">{item.value} colleges</p>
      </div>
    );
  }

  return null;
};

const CollegeTuitionVisualization = () => {
  const [colleges, setColleges] = useState<College[]>([]);

  useEffect(() => {
    CollegeAPI.getColleges({allItems: true}).then(res => {
      setColleges(res.results);
    });
  }, []);

  const collegesToData = () => {
    let data = [
      {name: '$0-$10k', value: 0},
      {name: '$10k-$20k', value: 0},
      {name: '$20k-$30k', value: 0},
      {name: '$30k-$40k', value: 0},
      {name: '$40k-$50k', value: 0},
      {name: '$50k-$60k', value: 0},
      {name: '$60k-$70k', value: 0},
      {name: '$70k-$80k', value: 0},
      {name: '$80k-$90k', value: 0},
      {name: '$90k-$100k', value: 0},
      {name: '$100k+', value: 0},
    ];
    colleges.forEach(college => {
      if (college.in_state_tuition < 10000) {
        data[0].value++;
      } else if (college.in_state_tuition < 20000) {
        data[1].value++;
      } else if (college.in_state_tuition < 30000) {
        data[2].value++;
      } else if (college.in_state_tuition < 40000) {
        data[3].value++;
      } else if (college.in_state_tuition < 50000) {
        data[4].value++;
      } else if (college.in_state_tuition < 60000) {
        data[5].value++;
      } else if (college.in_state_tuition < 70000) {
        data[6].value++;
      } else if (college.in_state_tuition < 80000) {
        data[7].value++;
      } else if (college.in_state_tuition < 90000) {
        data[8].value++;
      } else if (college.in_state_tuition < 100000) {
        data[9].value++;
      } else {
        data[10].value++;
      }
    });
    return data;
  };

  return (
    <PieChart width={350} height={350}>
      <Pie
        data={collegesToData()}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        innerRadius={60}
        outerRadius={120}
        fill="#6367F1"
        label
      />
      <Tooltip content={<CustomTooltip />} />
    </PieChart>
  );
};

export default CollegeTuitionVisualization;
