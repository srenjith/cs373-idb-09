import {
  CartesianGrid,
  Label,
  Bar,
  BarChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import {useEffect, useState} from 'react';
import dayjs from 'dayjs';
import EventAPI, {Event} from '../api/event';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-bold whitespace-nowrap">{item.name}</p>
        <p className="text-gray-900 whitespace-nowrap">
          Average Min Price: ${item.avgMinPrice.toFixed(2)}
        </p>
        <p className="text-gray-900 whitespace-nowrap">
          Month: {item.month}
        </p>
      </div>
    );
  }

  return null;
};

const DateMinPriceVisualization = () => {
  const [eventData, setEventData] = useState<Event[]>([]);

  useEffect(() => {
    getEventsSortedByMinPrice().then(data => {
      setEventData(data.results);
    });
  }, []);

  return (
    <BarChart
      width={550}
      height={350}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      }}
      data={eventsToDataPoints(eventData)}
    >
      <CartesianGrid />
      <XAxis dataKey="month" name="Month">
        <Label value="Month" offset={-10} position="insideBottom" />
      </XAxis>
      <YAxis
        type="number"
        dataKey="avgMinPrice"
        name="Minimum Price"
        unit="$"
        domain={[0, 75]}
      >
        <Label
          value="Minimum Price"
          offset={0}
          position="insideLeft"
          angle={-90}
        />
      </YAxis>
      <Tooltip content={<CustomTooltip />} cursor={{strokeDasharray: '3 3'}} />
      <Bar dataKey="avgMinPrice" name="Minimum Price" fill="#DFA82F" />
    </BarChart>
  );
};

const eventsToDataPoints = (events: Event[]) => {
  let data = [
    {month: 'Jan', avgMinPrice: 0, count: 0},
    {month: 'Feb', avgMinPrice: 0, count: 0},
    {month: 'Mar', avgMinPrice: 0, count: 0},
    {month: 'Apr', avgMinPrice: 0, count: 0},
    {month: 'May', avgMinPrice: 0, count: 0},
    {month: 'Jun', avgMinPrice: 0, count: 0},
    {month: 'Jul', avgMinPrice: 0, count: 0},
    {month: 'Aug', avgMinPrice: 0, count: 0},
    {month: 'Sep', avgMinPrice: 0, count: 0},
    {month: 'Oct', avgMinPrice: 0, count: 0},
    {month: 'Nov', avgMinPrice: 0, count: 0},
    {month: 'Dec', avgMinPrice: 0, count: 0},
  ];

  events.forEach(event => {
    const month = dayjs(event.start_date).format('MMM');
    const index = data.findIndex(item => item.month === month);
    data[index].avgMinPrice += event.min_price;
    data[index].count += 1;
  });

  data.forEach(item => {
    item.avgMinPrice = item.avgMinPrice / item.count;
  });
  return data;
};

const getEventsSortedByMinPrice = async () => {
  return await EventAPI.getEvents({
    sort: 'min_price',
    sortDirection: 'desc',
    allItems: true,
  });
};

export default DateMinPriceVisualization;
