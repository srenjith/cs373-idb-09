import {useEffect, useState} from 'react';
import {Restaurant} from '../api/restaurant';
import RestaurantAPI from '../api/restaurant';
import {
  CartesianGrid,
  XAxis,
  Label,
  YAxis,
  Tooltip,
  BarChart,
  Bar,
} from 'recharts';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-semibold whitespace-nowrap text-center">
          {item.price}
        </p>
        <p className="text-gray-900 whitespace-nowrap">
          Average Rating: {Math.round(item.averageRating * 100) / 100}
        </p>
      </div>
    );
  }

  return null;
};

const RestaurantPriceToRatingVisualization = () => {
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);

  useEffect(() => {
    RestaurantAPI.getRestaurants({allItems: true}).then(res => {
      setRestaurants(res.results);
    });
  }, []);

  const restaurantsToDataBars = () => {
    let data = [
      {price: '$', averageRating: 0, count: 0},
      {price: '$$', averageRating: 0, count: 0},
      {price: '$$$', averageRating: 0, count: 0},
      {price: '$$$$', averageRating: 0, count: 0},
    ];
    restaurants.forEach(restaurant => {
      data[restaurant.price - 1].averageRating += restaurant.rating;
      data[restaurant.price - 1].count++;
    });
    data.forEach(item => {
      item.averageRating = item.averageRating / item.count;
    });
    return data;
  };

  return (
    <BarChart
      width={350}
      height={350}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      }}
      data={restaurantsToDataBars()}
    >
      <CartesianGrid />
      <XAxis dataKey="price">
        <Label value="Price" offset={-10} position="insideBottom" />
      </XAxis>
      <YAxis domain={[0, 5]} tickCount={10}>
        <Label value="Average Rating" position="insideLeft" angle={-90} />
      </YAxis>
      <Tooltip content={<CustomTooltip />} cursor={{strokeDasharray: '3 3'}} />
      <Bar dataKey="averageRating" fill="#DFA82F" />
    </BarChart>
  );
};

export default RestaurantPriceToRatingVisualization;
