import {useEffect, useState} from 'react';
import {Pie, PieChart, Tooltip} from 'recharts';
import EventAPI, {Event} from '../api/event';

const CustomTooltip = ({active, payload, label}: any) => {
  if (active && payload && payload.length) {
    const item = payload[0].payload;
    return (
      <div className="bg-gray-100 border border-gray-400 rounded-lg p-2">
        <p className="text-gray-900 font-semibold whitespace-nowrap text-center">
          {item.name}
        </p>
        <p className="text-gray-900 whitespace-nowrap">
          {item.state} ({item.count})
        </p>
      </div>
    );
  }

  return null;
};

const EventsStatesVisualization = () => {
  const [events, setEvents] = useState<Event[]>([]);

  useEffect(() => {
    EventAPI.getEvents({allItems: true}).then(data => {
      setEvents(data.results);
    });
  }, []);

  const eventsToData = () => {
    let stateCounts: {[key: string]: number} = {};

    events.forEach(event => {
      if (event.state in stateCounts) {
        stateCounts[event.state] += 1;
      } else {
        stateCounts[event.state] = 1;
      }
    });

    const data = Object.keys(stateCounts)
      .filter(state => stateCounts[state] > 2)
      .map(state => {
        return {state: state, count: stateCounts[state]};
      });

    return data;
  };

  const data = eventsToData();

  return (
    <PieChart width={500} height={300}>
      <Pie
        data={data}
        dataKey="count"
        nameKey="state"
        cx="50%"
        cy="50%"
        innerRadius={50}
        outerRadius={90}
        fill="#6367F1"
        label={({
          cx,
          cy,
          midAngle,
          innerRadius,
          outerRadius,
          percent,
          index,
        }) => {
          const RADIAN = Math.PI / 180;
          const radius = 40 + innerRadius + (outerRadius - innerRadius);
          const x = cx + radius * Math.cos(-midAngle * RADIAN);
          const y = cy + radius * Math.sin(-midAngle * RADIAN);

          const state = data[index].state;
          const count = data[index].count;

          return (
            <text
              x={x}
              y={y}
              fill="black"
              textAnchor={x > cx ? 'start' : 'end'}
              dominantBaseline="central"
            >
              {state} ({count})
            </text>
          );
        }}
      />
      <Tooltip content={<CustomTooltip />} />
    </PieChart>
  );
};

export default EventsStatesVisualization;
