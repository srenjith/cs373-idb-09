import rashad from '../assets/rashad.jpg';
import susmita from '../assets/susmita.jpg';
import vrishank from '../assets/vrishank.png';
import zachary from '../assets/zachary.jpg';
import darren from '../assets/darren.jpg';

import {Tool} from '../components/toolCard';

type TeamMember = {
  name: string;
  username: string;
  email: string;
  role: string;
  bio: string;
  image: string;

  commits: number;
  issues: number;
  unitTests: number;
};

const teamInfo: TeamMember[] = [
  {
    name: 'Rashad Philizaire',
    username: 'rashadphil',
    email: 'rashadphilizaire@gmail.com', // needed for gitlab stats
    role: 'Backend',
    bio: "I am a second-year CS major at UT Austin and I'm from Alexandria Virginia. I enjoy playing chess and spikeball.",
    image: rashad,
    commits: 0,
    issues: 0,
    unitTests: 18,
  },
  {
    name: 'Susmita Renjith',
    username: 'srenjith',
    email: 'susmitarenjith@gmail.com',
    role: 'Frontend',
    bio: "I'm currently a second-year Computer Science major with a minor in Business. Outside of classes, I'm a part of UT's climbing team.",
    image: susmita,
    commits: 7, // counting commits from previous email
    issues: 0,
    unitTests: 21,
  },
  {
    name: 'Vrishank Viswanath',
    username: 'vrishank',
    email: 'vrishank@utexas.edu',
    role: 'Frontend',
    bio: "I'm a second-year CS major at UT from Dallas, TX. I love biking, cooking, and watching TV shows.",
    image: vrishank,
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
  {
    name: 'Zachary Guo',
    username: 'zachlguo',
    email: 'zacharyguo29@gmail.com',
    role: 'Backend',
    bio: 'I am a second-year CS major at UT from Houston. I enjoy playing basketball with friends.',
    image: zachary,
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
  {
    name: 'Darren Slack',
    username: 'Dartu42',
    email: 'dartu43@gmail.com',
    role: 'Backend',
    bio: "I am a third-year CS major at UT Austin from the Houston area. I'm part of the UT Dungeons and Dragons club and enjoy playing video games with friends.",
    image: darren,
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
];

const toolsInfo: Tool[] = [
  {
    name: 'React',
    image:
      'https://imgs.search.brave.com/hLReVb1S-Ou4Gru8lAAkc_X95RXnnTxL1LnfxDwWMqk/rs:fit:880:750:1/g:ce/aHR0cHM6Ly9yZXMu/Y2xvdWRpbmFyeS5j/b20vcHJhY3RpY2Fs/ZGV2L2ltYWdlL2Zl/dGNoL3MtLXFvX1dw/MzhaLS0vY19saW1p/dCUyQ2ZfYXV0byUy/Q2ZsX3Byb2dyZXNz/aXZlJTJDcV9hdXRv/JTJDd184ODAvaHR0/cHM6Ly9kZXYtdG8t/dXBsb2Fkcy5zMy5h/bWF6b25hd3MuY29t/L2kvZTBubDd6aXkx/bGE3YnB3ajdyc3Au/cG5n',
    description: 'A JavaScript library used to build user interfaces.',
    link: 'https://reactjs.org/',
  },
  {
    name: 'Flask',
    image:
      'https://imgs.search.brave.com/8cHJ9V_10hdjNEjgvR_rEit27Wo9uX6xUkfj6fsLpYI/rs:fit:860:681:1/g:ce/aHR0cHM6Ly93d3cu/a2luZHBuZy5jb20v/cGljYy9tLzE4OC0x/ODgyNDE2X2ZsYXNr/LXB5dGhvbi1sb2dv/LWhkLXBuZy1kb3du/bG9hZC5wbmc',
    description: 'A Python web framework used to create our website API.',
    link: 'https://flask.palletsprojects.com/en/2.2.x/',
  },
  {
    name: 'GitLab',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRXEORehd4csCTwe1HxwN9CfAO5eeL9iWWUMcC3oLpvxvLZgwKPdW5J82EU1XQ-NkX23ic&usqp=CAU',
    description:
      'A version control platform used to access repository statistics.',
    link: 'https://about.gitlab.com/',
  },
  {
    name: 'Postman',
    image:
      'https://images.squarespace-cdn.com/content/v1/57c649658419c2380d1947be/1534825375055-OA4431YN1BZ93RTAEIZF/postman-tile.png?format=1500w',
    description:
      'An API design and development platform used to create our website API.',
    link: 'https://www.postman.com/',
  },
  {
    name: 'Tailwind CSS',
    image:
      'https://res.cloudinary.com/arcjet-media/image/upload/v1608734952/z8hzeszc9eb3sp3vp3qc.jpg',
    description: 'A utility-first CSS framework used to style our website.',
    link: 'https://tailwindcss.com/',
  },
  {
    name: 'DaisyUI',
    image:
      'https://imgs.search.brave.com/I9IujP9a02MzNNLgqXyBUwvNGkygYF1J1bSswbnC4Qg/rs:fit:256:256:1/g:ce/aHR0cHM6Ly9pbWFn/ZXMub3BlbmNvbGxl/Y3RpdmUuY29tL2Rh/aXN5dWkvZDFlYjky/OC9sb2dvLzI1Ni5w/bmc',
    description: 'A Tailwind component library and theme builder',
    link: 'https://daisyui.com/',
  },
  {
    name: 'Headless UI',
    image:
      'https://imgs.search.brave.com/32bj5W7nPm794RoQ7Iqneo451xGmEuxY84fbM_w3drc/rs:fit:200:200:1/g:ce/aHR0cHM6Ly9pbWcu/c3RhY2tzaGFyZS5p/by9zZXJ2aWNlLzIx/MjIzL2RlZmF1bHRf/MzVlMDA1NTEwOTEx/NzExZDM1ZWRhZTI4/NDExNTFkZmI3NmYy/YjNmNS5wbmc',
    description:
      'A collection of fully accessible UI components, designed to integrate beautifully with Tailwind CSS.',
    link: 'https://headlessui.com/',
  },
];

const apisInfo: Tool[] = [
  {
    name: 'Yelp Fusion',
    image:
      'https://store-images.s-microsoft.com/image/apps.29817.9007199266243147.a52adf60-7105-4dcf-8722-08b1544c4ebc.d6ead9b4-4cd6-440f-8c44-18cfa043564f?mode=scale&q=90&h=300&w=300',
    description:
      "Yelp Fusion is an API that allows developers to access Yelp's database of businesses and reviews.",
    link: 'https://www.yelp.com/developers/',
  },
  {
    name: 'Ticketmaster API',
    image:
      'https://imgs.search.brave.com/Z99gkjytSDQczaSNjDvUYRmVxDxTg1zcxV5SKLf-Rkg/rs:fit:500:500:1/g:ce/aHR0cDovL3d3dy5y/YW5rbG9nb3MuY29t/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDE2/LzA5L1RpY2tldG1h/c3Rlci5jb20tTG9n/by1STDExMzMtNTAw/eDUwMC5wbmc',
    description:
      'The Ticketmaster Discovery API is an API that allows you to search for events and attractions.',
    link: 'https://developer.ticketmaster.com/',
  },
  {
    name: 'College Scorecard API',
    image: 'https://collegescorecard.ed.gov/img/US-DeptOfEducation-Seal.png',
    description:
      'The College Scorecard API is an API that allows you to search for data on colleges and universities.',
    link: 'https://collegescorecard.ed.gov/data/documentation/',
  },
  {
    name: 'Bing Image Search API',
    image:
      'https://cdn.vox-cdn.com/thumbor/lw7eaG_tnqPDo-Jy5CVzQ22WyCY=/0x0:660x440/1400x1050/filters:focal(330x220:331x221)/cdn.vox-cdn.com/uploads/chorus_asset/file/21937385/binglogo.jpg',
    description:
      'The Bing Image Search API allowed us to populate the college images on the website.',
    link: 'https://docs.microsoft.com/en-us/bing/search-apis/bing-image-search/overview',
  },
];

export {teamInfo, toolsInfo, apisInfo};
export type {TeamMember};
