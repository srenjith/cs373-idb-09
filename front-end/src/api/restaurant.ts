import {Metadata} from './API';
import {College} from './college';
import {api} from './configs/axiosConfig';
import {QueryParams, buildQueryString} from './queryBuilder';

export type Restaurant = {
  id: number;
  price: number;
  rating: number;
  city: string;
  address: string;
  state: string;
  phone: string;
  latitude: number;
  category: string;
  name: string;
  image: string;
  url: string;
};

export type RestaurantSortable = 'name' | 'rating' | 'price';

export type RestaurantExactFilterableFields =
  | 'city'
  | 'state'
  | 'category'
  | 'price';

export type RestaurantRangeFilterableFields = 'rating' | 'price';

type GetRestaurantsResponse = {
  metadata: Metadata;
  results: Restaurant[];
};

export type NearbyEventsResponse = {
  distance: number;
  event: Event;
}[];

export type NearbyCollegeResponse = {
  distance: number;
  college: College;
}[];

const RestaurantAPI = {
  getRestaurants: async ({
    page = 1,
    perPage = 12,
    sort = 'name',
    sortDirection = 'asc',
    exactFilters = {},
    rangeFilters = {},
    search = undefined,
    allItems = false,
  }: QueryParams<
    RestaurantSortable,
    RestaurantExactFilterableFields,
    RestaurantRangeFilterableFields
  >) => {
    const queryString = buildQueryString({
      page,
      perPage,
      sort,
      sortDirection,
      exactFilters,
      rangeFilters,
      search,
      allItems,
    });

    const response = await api.get<GetRestaurantsResponse>(
      `/restaurants?${queryString}`
    );
    return response.data;
  },

  getRestaurant: async (id: number) => {
    const response = await api.get<Restaurant>(`/restaurants/${id}`);
    return response.data;
  },

  getNearbyEvents: async (id: number) => {
    const response = await api.get<NearbyEventsResponse>(
      `/restaurants/${id}/events`
    );
    return response.data;
  },

  getNearbyColleges: async (id: number) => {
    const response = await api.get<NearbyCollegeResponse>(
      `/restaurants/${id}/colleges`
    );
    return response.data;
  },
};

export default RestaurantAPI;
