import axios from 'axios';

type ProviderResponse = {
  items: ProviderCollege[];
  numItems: number;
  page: number;
  pages: number;
};

export type ProviderCollege = {
  admission_rate: number;
  cips: number[];
  city: string;
  id: number;
  image: null | string;
  locale: number;
  men_only: boolean;
  name: string;
  price_calc_url: string;
  sat_avg: number;
  school_url: string;
  state: string;
  studentBodySize: number;
  women_only: boolean;
  zipCode: string;
};

const getCollegesSortedBySAT = async () => {
  const ENDPOINT_URL =
    'https://api.easycollegesearch.me/colleges?sort_field=sat_avg&sort_order=desc';
  const response = await axios.get<ProviderResponse>(ENDPOINT_URL);
  return response.data.items;
};

const getCollegesSortedByStudentBodySize = async () => {
  const ENDPOINT_URL =
    'https://api.easycollegesearch.me/colleges?sort_field=studentBodySize&sort_order=desc';
  const response = await axios.get<ProviderResponse>(ENDPOINT_URL);
  return response.data.items;
};

export {getCollegesSortedBySAT, getCollegesSortedByStudentBodySize};
