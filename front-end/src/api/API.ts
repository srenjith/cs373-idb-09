import {College} from './college';
import {Restaurant} from './restaurant';
import {Event} from './event';

import RestaurantAPI from './restaurant';
import CollegeAPI from './college';
import EventAPI from './event';

export type Metadata = {
  page: number;
  per_page: number;
  total_items: number;
  total_pages: number;
};

export type NearbyRestaurantsResponse = {
  distance: number;
  restaurant: Restaurant;
}[];

export type NearbyEventsResponse = {
  distance: number;
  event: Event;
}[];

export type NearbyCollegeResponse = {
  distance: number;
  college: College;
}[];

export {RestaurantAPI, CollegeAPI, EventAPI};
export type {Restaurant, College, Event};
