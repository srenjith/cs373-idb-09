import {
  Metadata,
  NearbyCollegeResponse,
  NearbyEventsResponse,
  NearbyRestaurantsResponse,
} from './API';
import {api} from './configs/axiosConfig';
import {QueryParams, buildQueryString} from './queryBuilder';

export type Event = {
  id: number;
  name: string;
  url: string;
  image: string;
  classification: string;
  country: string;
  state: string;
  city: string;
  venue_name: string;
  latitude: number;
  longitude: number;
  min_price: number;
  max_price: number;
  start_date: string;
};

type GetEventsResponse = {
  metadata: Metadata;
  results: Event[];
};

export type EventSortable =
  | 'name'
  | 'venue_name'
  | 'start_date'
  | 'min_price'
  | 'max_price';

export type EventExactFilterableFields =
  | 'city'
  | 'state'
  | 'venue_name'
  | 'country'
  | 'start_date'
  | 'classification';

export type EventRangeFilterableFields =
  | 'min_price'
  | 'max_price'
  | 'start_date';

const EventAPI = {
  getEvents: async ({
    page = 1,
    perPage = 12,
    sort = 'name',
    sortDirection = 'asc',
    exactFilters = {},
    rangeFilters = {},
    search = undefined,
    allItems = false,
  }: QueryParams<
    EventSortable,
    EventExactFilterableFields,
    EventRangeFilterableFields
  >) => {
    const queryString = buildQueryString({
      page,
      perPage,
      sort,
      sortDirection,
      exactFilters,
      rangeFilters,
      search,
      allItems,
    });
    const response = await api.get<GetEventsResponse>(`/events?${queryString}`);
    return response.data;
  },

  getEvent: async (id: number) => {
    const response = await api.get(`/events/${id}`);
    return response.data;
  },

  getNearbyRestaurants: async (id: number) => {
    const response = await api.get<NearbyRestaurantsResponse>(
      `/events/${id}/restaurants`
    );
    return response.data;
  },

  getNearbyColleges: async (id: number) => {
    const response = await api.get<NearbyCollegeResponse>(
      `/events/${id}/colleges`
    );
    return response.data;
  },
};

export default EventAPI;
