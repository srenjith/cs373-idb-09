import axios, {AxiosError} from 'axios';

export const BASE_URL = 'https://api.campuseatsandtreats.me';

const api = axios.create({
  baseURL: BASE_URL,
});

const errorHandle = (error: AxiosError) => {
  const statusCode = error.response?.status;
  if (statusCode) {
    console.error(error.response?.data);
    console.log(error.response?.status);
  }

  return Promise.reject(error);
};

api.interceptors.request.use(request => request, errorHandle);

export {api};
