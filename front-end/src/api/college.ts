import {Metadata, NearbyEventsResponse, NearbyRestaurantsResponse} from './API';
import {api} from './configs/axiosConfig';
import {QueryParams, buildQueryString} from './queryBuilder';

export type College = {
  id: number;
  name: string;
  city: string;
  state: string;
  in_state_tuition: number;
  out_of_state_tuition: number;
  admission_rate: number;
  sat_score: number;
  size: number;
  latitude: number;
  longitude: number;
  image: string;
};

type GetCollegesResponse = {
  metadata: Metadata;
  results: College[];
};

export type CollegeSortable =
  | 'name'
  | 'admission_rate'
  | 'size'
  | 'in_state_tuition'
  | 'out_of_state_tuition'
  | 'sat_score';

export type CollegeExactFilterableFields = 'state' | 'city';

export type CollegeRangeFilterableFields =
  | 'admission_rate'
  | 'size'
  | 'in_state_tuition'
  | 'out_of_state_tuition'
  | 'sat_score';

const CollegeAPI = {
  getColleges: async ({
    page = 1,
    perPage = 12,
    sort = 'sat_score',
    sortDirection = 'desc',
    exactFilters = {},
    rangeFilters = {},
    search = undefined,
    allItems = false,
  }: QueryParams<
    CollegeSortable,
    CollegeExactFilterableFields,
    CollegeRangeFilterableFields
  >) => {
    if (typeof rangeFilters.max?.admission_rate === 'number') {
      rangeFilters.max.admission_rate += 0.005;
    }

    const queryString = buildQueryString({
      page,
      perPage,
      sort,
      sortDirection,
      exactFilters,
      rangeFilters,
      search,
      allItems,
    });

    const response = await api.get<GetCollegesResponse>(
      `/colleges?${queryString}`
    );
    return response.data;
  },

  getCollege: async (id: number) => {
    const response = await api.get<College>(`/colleges/${id}`);
    return response.data;
  },

  getNearbyRestaurants: async (id: number) => {
    const response = await api.get<NearbyRestaurantsResponse>(
      `/colleges/${id}/restaurants`
    );
    return response.data;
  },

  getNearbyEvents: async (id: number) => {
    const response = await api.get<NearbyEventsResponse>(
      `/colleges/${id}/events`
    );
    return response.data;
  },
};

export default CollegeAPI;
