# Referenced from https://gitlab.com/sriyab/workitout/-/blob/main/front-end/guitest.py
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# URL depends on if you're testing locally or not
# url = "http://localhost:5173/"
url = "https://campuseatsandtreats.web.app/"

class GuiTests (unittest.TestCase):
  def setUp (self):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--window-size=1280,800")
    self.driver = webdriver.Chrome(options=chrome_options)
        
  def tearDown (self):
    self.driver.quit()

  # Test 1
  def test_a (self):
    # Navigate to About
    self.driver.get(url)
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]/div[2]/ul/li[2]/a")
    button.click()
    self.assertEqual(self.driver.current_url, url + "about")

    # Navigate to Home
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]/div[2]/ul/li[1]/a")
    button.click()
    self.assertEqual(self.driver.current_url, url)

    # Test model pages
    options = ["colleges", "restaurants", "events"]
    for x in range(1, 4):
      # Start at home page every time
      # self.driver.get(url)

      dropdown = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]/div[2]/ul/li[3]/a")
      submenu = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]/div[2]/ul/li[3]/ul/li[" + str(x) + "]/a")

      actions = ActionChains(self.driver)
      actions.move_to_element(dropdown)
      actions.click(submenu)
      actions.perform()
      
      self.assertEqual(self.driver.current_url, url + options[x - 1])

  # Test 2
  def test_b (self):
    self.driver.get(url + "colleges")

    # Wait for the image to load before clicking
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    card = self.driver.find_element(By.XPATH, "//*[@id='root']/div/section/div/div[2]/div[1]")
    card.click()

    instance = self.driver.find_element(By.ID, "headlessui-portal-root")
    self.assertEqual(True, instance.is_displayed())

  # Test 3
  def test_c (self):
    self.driver.get(url + "restaurants")

    # Wait for the image to load before clicking
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    card = self.driver.find_element(By.XPATH, "//*[@id='root']/div/section/div/div[2]/div[1]")
    card.click()

    instance = self.driver.find_element(By.ID, "headlessui-portal-root")
    self.assertEqual(True, instance.is_displayed())

  # Test 4
  def test_d (self):
    self.driver.get(url + "events")

    # Wait for the image to load before clicking
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    card = self.driver.find_element(By.XPATH, "//*[@id='root']/div/section/div/div[2]/div[1]")
    card.click()

    instance = self.driver.find_element(By.ID, "headlessui-portal-root")
    self.assertEqual(True, instance.is_displayed())

  # Test 5
  def test_e (self):
    self.driver.get(url + "about")
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]/div[1]/a")
    button.click()

    self.assertEqual(self.driver.current_url, url)

  # Test 6
  def test_f (self):
    self.driver.get(url + "about")
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[2]/div[3]/div[1]/div/div[2]/a[1]")
    while (True):
      # Keep trying button click until success
      try:
        button.click()
        break
      except:
        pass

    actions = ActionChains(self.driver)
    actions.move_to_element(button)
    actions.click(button)
    actions.perform()

    # Clicking the button opens a new tab, make sure we're on the right one
    tabs = self.driver.window_handles
    self.driver.switch_to.window(tabs[1])
    
    self.assertEqual(self.driver.current_url, "https://gitlab.com/srenjith/cs373-idb-09")

  # Test 7
  def test_g (self):
    self.driver.get(url + "about")
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[2]/div[3]/div[1]/div/div[2]/a[2]")
    while (True):
      # Keep trying button click until success
      try:
        button.click()
        break
      except:
        pass

    # Clicking the button opens a new tab, make sure we're on the right one
    tabs = self.driver.window_handles
    self.driver.switch_to.window(tabs[1])

    self.assertEqual(self.driver.current_url, "https://documenter.getpostman.com/view/18944196/2s93CExca5")

  # Test 8
  def test_h (self):
    self.driver.get(url + "#explore")

    # Try clicking the colleges button
    button = self.driver.find_element(By.XPATH, "//*[@id='explore']/div/div[2]/a[1]")
    try:
      button.click()
    except:
      button.click()

    self.assertEqual(self.driver.current_url, url + "colleges")

  # Test 9
  def test_i (self):
    self.driver.get(url + "#explore")

    # Try clicking the restaurants button
    button = self.driver.find_element(By.XPATH, "//*[@id='explore']/div/div[2]/a[2]")
    try:
      button.click()
    except:
      button.click()

    self.assertEqual(self.driver.current_url, url + "restaurants")

  # Test 10
  def test_j (self):
    self.driver.get(url)
    button = self.driver.find_element(By.XPATH, "//*[@id='root']/div/div[2]/div/div/div[2]/a/button")
    try:
      button.click()
    except:
      button.click()

    # The click should have only scrolled us down
    self.assertEqual(self.driver.current_url, url + "#explore")

    # Try clicking the events button
    button = self.driver.find_element(By.XPATH, "//*[@id='explore']/div/div[2]/a[3]")
    try:
      button.click()
    except:
      button.click()

    self.assertEqual(self.driver.current_url, url + "events")

  # Test 11
  def test_k (self):
    # Test searching, sorting, filtering for colleges
    self.driver.get(url + "colleges")
    
    # Wait for the instances to load
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Test searching
    search_bar = search_bar = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/div[1]/input")
    search_bar.send_keys("Austin")

    # UT Austin should be displayed
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    ut = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")
    self.assertEqual(True, ut.is_displayed())

    # Texas A&M should NOT be displayed
    tamu = self.driver.find_elements(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[3]/div/img")
    self.assertEqual(True, len(tamu) == 0)

    # Test sorting
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
 
    sort = self.driver.find_element(By.ID, "headlessui-listbox-button-:r0:")
    sort.click()
    size = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-listbox-option-:rb:\"]")
    size.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Arizona State University isn't displayed on the first page until sorting by size
    asu = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div")
    self.assertEqual(True, asu.is_displayed())

    # Test filtering
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    filt = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/div[5]/button")
    filt.click()
    WebDriverWait(self.driver, 60).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"headlessui-popover-panel-:re:\"]/div/div[1]/div/div[2]/input")))
    rate_high = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-popover-panel-:re:\"]/div/div[1]/div/div[2]/input")
    rate_high.clear()
    rate_high.send_keys(6)

    princeton = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[2]/div")
    self.assertEqual(True, princeton.is_displayed())

  # Test 12
  def test_l (self):
    # Test searching, sorting, filtering for restaurants
    self.driver.get(url + "restaurants")
    
    # Wait for the instances to load
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Test searching
    search_bar = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/div[1]/input")
    search_bar.send_keys("Taco")

    # A taco restaurant should be displayed
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    taco = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")
    self.assertEqual(True, taco.is_displayed())

    # Test sorting
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
 
    sort = self.driver.find_element(By.ID, "headlessui-listbox-button-:r0:")
    sort.click()
    size = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-listbox-option-:r9:\"]")
    size.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Noonie's Deli should appear when sorting by price
    deli = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div")
    self.assertEqual(True, deli.is_displayed())

    # Test filtering
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    filt = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-popover-button-:r4:\"]")
    filt.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"headlessui-popover-panel-:rb:\"]/div/div[1]/div/div[1]/input")))
    rate_low = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-popover-panel-:rb:\"]/div/div[1]/div/div[1]/input")
    rate_low.send_keys(4)

    market = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[2]/div")
    self.assertEqual(True, market.is_displayed())

  # Test 13
  def test_m (self):
    # Test searching, sorting, filtering for colleges
    self.driver.get(url + "events")
    
    # Wait for the instances to load
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Test searching
    search_bar = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/div[1]/input")
    search_bar.send_keys("Mavericks")

    # A few Dallas Mavericks games should be displayed
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
    game = self.driver.find_elements(By.CSS_SELECTOR, "img")
    self.assertEqual(True, len(game) >= 1) # There should be at least 1 result

    # Test sorting
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))
 
    sort = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-listbox-button-:r0:\"]")
    sort.click()
    size = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-listbox-option-:rb:\"]")
    size.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    # Another game should be displayed
    game = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div")
    self.assertEqual(True, game.is_displayed())

    # Test filtering
    reset = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/button")
    reset.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/section/div/div[2]/div[1]/div/img")))

    filt = self.driver.find_element(By.XPATH, "//*[@id=\"root\"]/div/section/div/div[1]/div[2]/div[6]/button")
    filt.click()
    WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"headlessui-popover-panel-:rd:\"]/div/div[1]/div/div[2]/input")))
    rate_high = self.driver.find_element(By.XPATH, "//*[@id=\"headlessui-popover-panel-:rd:\"]/div/div[1]/div/div[1]/input")
    rate_high.send_keys(1)

    game = self.driver.find_elements(By.CSS_SELECTOR, "img")
    self.assertEqual(True, len(game) > 0)

if __name__ == "__main__":
    unittest.main()