SHELL := bash

dev-f:
	@echo "Starting frontend"
	cd front-end && npm run dev -- --host --open

deploy:
	@echo "Deploying to Firebase"
	cd front-end && npm run build && firebase deploy

all:

format:
	@echo "Formatting frontend"
	cd front-end && npx prettier --write .

	@echo "Formatting backend"
	black back-end

back-end-tests:
	@echo "Running backend tests"
	cd back-end && python3 -m unittest tests.py